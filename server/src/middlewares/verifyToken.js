import jwt from 'jsonwebtoken'

export const verifyToken = (req, res, next) => {
    const accessToken = req.headers.authorization?.split(' ')[1]
    if (!accessToken) {
        return res.status(401).json({
            err: 1,
            msg: "Missing access token"
        })
    }

    jwt.verify(accessToken, process.env.ACCESS_KEY, (err, user) => {
        if (err) return res.status(401).json({
            err: 1,
            msg: "Access token expired"
        })
        req.user = user
        next()
    })

}