import db from '../models';


export const getCurrentUserService = (id) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.User.findOne({

            where: { id },
            raw: true,
            attributes: {
                exclude: ['password', 'role']
            }
        })

        resolve({
            err: respone ? 0 : 1,
            msg: respone ? 'Get user successfully!' : 'Failed to get user',
            respone
        })



    } catch (error) {
        reject(error);
    }
})
export const getAllUserService = () => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.User.findAll({


            raw: true,
            attributes: {
                exclude: ['password', 'role']
            }
        })

        resolve({
            err: 0,
            respone
        })



    } catch (error) {
        reject(error);
    }
})
export const editUserService = (data) => new Promise(async (resolve, reject) => {

    try {

        const respone = await db.User.findOne({

            where: { id: data.id },
            raw: false,

        })
        if (respone) {
            respone.firstName = data.firstName
            respone.lastName = data.lastName
            respone.address = data.address
            respone.phoneNumber = data.phoneNumber
            await respone.save()
            resolve({
                err: 0,
                msg: "Update successfully!",
            })
        } else {
            resolve({
                err: 0,
                msg: "User not found"
            })
        }




    } catch (error) {
        reject(error);
    }
})

