import db from '../models';


export const addProductService = ({ category_id, product_name, product_img, product_price, product_calories }) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.Product.findOrCreate({

            where: { product_name },
            defaults: {
                category_id, product_name, product_img, product_price, product_calories
            }
        })
        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra token

        resolve({
            err: respone[1] ? 0 : 2,
            msg: respone[1] ? 'Add product successfully!!' : 'Already product exist!',

        })



    } catch (error) {
        reject(error);
    }
})


export const getProductCategoryService = (params) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Product.findAll({


            where: { category_id: params.category }
        })
        resolve({

            response
        })



    } catch (error) {
        reject(error);
    }
})
export const getProductByIdService = (params) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Product.findOne({

            where: { id: params.id },
            raw: true

        })
        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra token

        resolve({
            err: response ? 0 : 2,
            msg: response ? 'Get food successfully!!' : 'Food not exist!',
            response
        })



    } catch (error) {
        reject(error);
    }
})
export const getAllProductService = () => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Product.findAll({
            raw: true,
            nest: true,
            include: [{ model: db.Category, as: "category", attributes: ["category_name"] }]

        })

        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra token
        resolve({

            response
        })



    } catch (error) {
        reject(error);
    }
})
export const updateProductService = (data) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.Product.findOne({


            where: { id: data.id },
            raw: false,

        })

        if (respone) {
            respone.product_name = data.product_name
            respone.category_id = data.category_id
            respone.product_img = data.product_img
            respone.product_price = data.product_price
            respone.product_calories = data.product_calories
            await respone.save()
            resolve({
                err: 0,
                msg: "Update successfully!",

            })
        } else {
            resolve({
                err: 1,
                msg: "Product not found"
            })
        }



    } catch (error) {
        reject(error);
    }
})
export const deleteProductService = (id) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.Product.destroy({


            raw: true,
            where: { id },

        })


        resolve({
            err: 0,
            msg: "Delete Food Successfully!"
        })



    } catch (error) {
        reject(error);
    }
})

