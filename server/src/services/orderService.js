import db from '../models';
require("dotenv").config()

export const orderService = (user_id, dataCart) => new Promise(async (resolve, reject) => {
    const { cart, totalPaid, valuePaymentMethod, address } = dataCart
    try {

        const addDataOrder = await db.Order.create({


            user_id,
            status: "processing",
            total: totalPaid,
            payment_method: valuePaymentMethod,
            address
        })
        await cart.map((itemCart) => {
            return db.OrderItem.create({

                order_id: addDataOrder.toJSON().id,
                product_id: itemCart.id,
                quantity: itemCart.qty
            })
        })


        resolve({
            err: 0,
            msg: "Check out successfully!!",
        })



    } catch (error) {
        reject(error);
    }
})
export const getOrderUserService = (user_id) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Order.findAll({

            raw: true,
            nest: true,
            where: { user_id: user_id.uid },


        })
        resolve({
            response
        })

    } catch (error) {
        reject(error);
    }
})
export const getAllOrderService = () => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Order.findAll({

            nest: true,
            raw: true,
            attributes: ["id", "total", "status", "payment_method", "address", "createdAt"],
            include: [{ model: db.User, as: "users", attributes: ["firstName", "lastName", "email", "phoneNumber"] }]

        })
        resolve({
            err: 0,
            response
        })

    } catch (error) {
        reject(error);
    }
})
export const getOrderLimitService = (limit) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Order.findAll({

            nest: true,
            limit: 5,
            raw: true,
            order: [
                ['createdAt', 'DESC'],

            ],
            attributes: ["id", "total", "status", "payment_method", "address"],
            include: [{ model: db.User, as: "users", attributes: ["firstName", "lastName", "email", "phoneNumber"] }]

        })
        resolve({
            err: 0,
            response
        })

    } catch (error) {
        reject(error);
    }
})
export const getOrderDetailsService = (order_id) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Order.findAll({

            raw: true,
            nest: true,
            where: { id: order_id },
            attributes: [],
            // include: [{ model: db.Product }, { model: db.User, as: "users" }]
            include: [{ model: db.Product, attributes: ["id", "product_name", "product_img", "product_price", "product_calories"] }]


        })

        const total = await db.Order.findOne({
            raw: true,
            nest: true,
            where: { id: order_id },
            attributes: ["payment_method", "total", "status", "id", "address"],

        })

        resolve({
            response, total

        })



    } catch (error) {
        reject(error);
    }
})
export const deleteOrderDetailsService = (order_id) => new Promise(async (resolve, reject) => {
    try {



        const order = await db.Order.destroy({
            raw: true,
            where: { id: order_id },

        })

        const orderItem = await db.OrderItem.destroy({
            where: { order_id },

        })

        resolve({
            err: 0,
            msg: "Delete Order Successfully!"
        })



    } catch (error) {
        reject(error);
    }
})
export const editStatusOrderService = (data) => new Promise(async (resolve, reject) => {
    console.log('data: ', data);
    try {



        const respone = await db.Order.findOne({

            where: { id: data.id },
            raw: false,

        })



        if (respone) {
            respone.status = data.status

            await respone.save()
            resolve({
                err: 0,
                msg: "Order is updated!",
            })
        }



    } catch (error) {
        reject(error);
    }
})



