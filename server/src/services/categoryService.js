import db from '../models';


export const addCategoryService = ({ category_name }) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.Category.findOrCreate({

            where: { category_name },
            defaults: {
                category_name
            }
        })
        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra add thanh cong

        resolve({
            err: respone[1] ? 0 : 2,
            msg: respone[1] ? 'Add category successfully!!' : 'Already category exist!',

        })



    } catch (error) {
        reject(error);
    }
})
export const getCategoryService = () => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Category.findAll()
        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra add thanh cong

        resolve({
            response
        })



    } catch (error) {
        reject(error);
    }
})
export const editCategoryService = (data) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.Category.findOne({

            where: { id: data.id },
            raw: false,
        })
        if (response) {
            response.category_name = data.category_name

            await response.save()
            resolve({
                err: 0,
                msg: "Update successfully!",

            })
        } else {
            resolve({
                err: 1,
                msg: "Category not found"
            })
        }

    } catch (error) {
        reject(error);
    }
})
export const deleteCategoryService = (id) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.Category.destroy({


            raw: true,
            where: { id },

        })
        resolve({
            err: 0,
            msg: "Delete Category Successfully!"
        })

    } catch (error) {
        reject(error);
    }
})

