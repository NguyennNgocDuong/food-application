import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import db from '../models';
require("dotenv").config()


const hashPassword = password => bcrypt.hashSync(password, bcrypt.genSaltSync(10))

const generateAccessToken = (user) => {
    return jwt.sign({ id: user.id, role: user.role }, process.env.ACCESS_KEY, { expiresIn: '30d' })
}
const generateRefreshToken = (user) => {
    return jwt.sign({ id: user.id, role: user.role }, process.env.REFRESH_KEY, { expiresIn: '365d' })
}

export const registerService = ({ firstName, lastName, email, password }) => new Promise(async (resolve, reject) => {
    try {

        const respone = await db.User.findOrCreate({

            where: { email },
            defaults: {
                firstName, lastName, email, password: hashPassword(password), role: "user"
            }
        })
        // respone trả về là 1 mảng. Nếu phần tử thứ 2 của mảng là true(người dùng chưa tồn tại) thì sẽ sinh ra token
        // const token = respone[1] && jwt.sign({ id: respone[0].id, email: respone[0].email }, process.env.SECRET_KEY, { expiresIn: '2d' })
        resolve({
            err: respone[1] ? 0 : 2,
            msg: respone[1] ? 'Register is successfully!' : 'Account is already registered!',
            // token: token || null
        })



    } catch (error) {
        reject(error);
    }
})

export const loginService = ({ email, password }) => new Promise(async (resolve, reject) => {
    try {

        const response = await db.User.findOne({
            where: { email },
            raw: true,

        })



        const isCorrectPassword = response && bcrypt.compareSync(password, response.password)
        const accessToken = isCorrectPassword && generateAccessToken(response)
        const refreshToken = isCorrectPassword && generateRefreshToken(response)



        resolve({
            err: accessToken ? 0 : 2,
            msg: accessToken ? 'Login is successfully!' : response ? 'Password is incorrect!' : 'Account is not exist! Please register',

            user: { ...response, accessToken, refreshToken }

        })



    } catch (error) {
        reject(error);
    }
})
export const refreshTokenService = ({ id }) => new Promise(async (resolve, reject) => {
    try {
        const response = await db.User.findOne({
            where: { id },
            raw: true,

        })

        const newAccessToken = generateAccessToken(response)
        const newRefreshToken = generateRefreshToken(response)

        resolve({


            accessToken: newAccessToken,
            refreshToken: newRefreshToken

        })



    } catch (error) {
        reject(error);
    }
})