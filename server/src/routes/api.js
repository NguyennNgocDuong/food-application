import express from "express"
import * as authController from "../controller/authController"
import * as userController from "../controller/userController"
import * as productController from "../controller/productController"
import * as categoryController from "../controller/categoryController"
import * as orderController from "../controller/orderController"
import * as paymentController from "../controller/paymentController"
import { upload } from "../utils/configUploadFile"
import { verifyToken } from "../middlewares/verifyToken"


const router = express.Router()

const initApiRoutes = (app) => {
    // auth
    router.post('/register', authController.register)
    router.post('/login', authController.login)
    router.post('/refresh-token', authController.refreshToken)

    // user
    router.get('/get-current-user', verifyToken, userController.getCurrentUser)
    router.get('/get-all-user', userController.getAllUser)
    router.put('/edit-user', verifyToken, userController.editUser)


    // product
    router.post('/add-product', upload.single("product_img"), productController.addProduct)
    router.get('/get-product/:category', productController.getProductCategory)
    router.get('/get-product-id/:id', productController.getProductById)
    router.get('/get-all-product', productController.getAllProduct)
    router.put('/update-product', upload.single("product_img"), productController.updateProduct)
    router.delete('/delete-product/:id', productController.deleteProduct)


    // category
    router.post('/add-category', categoryController.addCategory)
    router.get('/get-category', categoryController.getAllCategories)
    router.put('/edit-category', categoryController.editCategories)
    router.delete('/delete-category/:id', categoryController.deleteCategories)

    // order
    router.post('/check-out', verifyToken, orderController.order)
    router.get('/get-order-user/:uid', orderController.getOrderUser)
    router.get('/get-order-detail/:id', orderController.getOrderDetails)
    router.get('/get-all-order', orderController.getAllOrder)
    router.get('/get-order-limit/:limit', orderController.getOrderLimit)
    router.delete('/delete-order/:id', orderController.deleteOrderDetails)
    router.put('/edit-status-order', orderController.editStatusOrder)

    // payment
    router.get('/client-id-paypal', paymentController.payment)







    return app.use('/api/v1', router)
}

export default initApiRoutes