'use strict';

module.exports = {

  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      firstName: 'Duong',
      lastName: 'Nguyen',
      email: 'duong@gmail.com',
      address: 'TPHCM',
      phoneNumber: '0775771839',
      gender: 1,
      roleId: 'R1',
      image: null,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
