import express from "express"
import { connectDB } from "./configs/connectDB"
import configViewEngine from "./configs/viewEngine"
import initApiRoutes from "./routes/api"
import cors from 'cors'
import path from "path";



require("dotenv").config()
const app = express()
app.use(cors({ origin: process.env.CLIENT_URL, methods: ['POST', 'GET', 'DELETE', 'PUT'] }))
const port = process.env.PORT || 8888

// middleware
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')))


// setup view engine
configViewEngine(app)
initApiRoutes(app)

connectDB()

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})