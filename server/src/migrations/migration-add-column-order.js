'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Orders', 'status', {
      type: Sequelize.ENUM('processing', 'shipping', 'delieverd', 'cancel'),
      defaultValue: 'processing'
    })
    await queryInterface.addColumn('Orders', 'total', {
      type: Sequelize.INTEGER,
    })
    await queryInterface.addColumn('Orders', 'payment_method', {
      type: Sequelize.STRING,
    })
    await queryInterface.addColumn('Orders', 'address', {
      type: Sequelize.STRING,
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Orders', "status");
    await queryInterface.removeColumn('Orders', "total");
    await queryInterface.removeColumn('Orders', "payment_method");
    await queryInterface.removeColumn('Orders', "address");
  }
};