'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here-

    }
  };
  OrderItem.init({
    order_id: { type: DataTypes.INTEGER, references: { model: "Order", key: "id" } },
    product_id: { type: DataTypes.INTEGER, references: { model: "Product", key: "id" } },
    quantity: DataTypes.INTEGER,


  }, {
    sequelize,
    modelName: 'OrderItem',
  });
  return OrderItem;
};