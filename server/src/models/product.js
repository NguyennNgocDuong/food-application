'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.belongsTo(models.Category, { foreignKey: "category_id", targetKey: "id", as: "category" })

      Product.belongsToMany(models.Order, { through: models.OrderItem, foreignKey: "product_id" })

    }
  };
  Product.init({
    category_id: DataTypes.INTEGER,
    product_name: DataTypes.STRING,
    product_img: DataTypes.STRING,
    product_price: DataTypes.INTEGER,
    product_calories: DataTypes.INTEGER,

  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};