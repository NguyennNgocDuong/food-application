'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here-
      Order.belongsTo(models.User, { foreignKey: "user_id", targetKey: "id", as: "users" })
      Order.belongsToMany(models.Product, { through: models.OrderItem, foreignKey: "order_id" })

    }
  };
  Order.init({
    user_id: DataTypes.INTEGER,
    status: DataTypes.ENUM('processing', 'shipping', 'delieverd', 'cancel'),
    total: DataTypes.INTEGER,
    payment_method: DataTypes.STRING,
    address: DataTypes.STRING,



  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};