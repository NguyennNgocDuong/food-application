import * as categoryService from '../services/categoryService'


export const addCategory = async (req, res, next) => {


    try {
        const response = await categoryService.addCategoryService(req.body)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at category controller: ' + error })

    }
}
export const getAllCategories = async (req, res, next) => {


    try {
        const response = await categoryService.getCategoryService()
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at category controller: ' + error })

    }
}
export const editCategories = async (req, res, next) => {


    try {
        const response = await categoryService.editCategoryService(req.body)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at category controller: ' + error })

    }
}
export const deleteCategories = async (req, res, next) => {
const {id}= req.params

    try {
        const response = await categoryService.deleteCategoryService(id)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at category controller: ' + error })

    }
}