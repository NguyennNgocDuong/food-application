import * as paymentService from '../services/paymentService'

export const payment = async (req, res) => {

    try {
        const response = await paymentService.paymentService()
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at payment controller: ' + error })

    }
}


