import * as productService from '../services/productService'


export const addProduct = async (req, res, next) => {
    const { filename } = req.file

    try {
        const response = await productService.addProductService({ ...req.body, product_img: filename })
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}

export const getProductCategory = async (req, res, next) => {

    try {
        const response = await productService.getProductCategoryService(req.params)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}
export const getProductById = async (req, res, next) => {

    try {
        const response = await productService.getProductByIdService(req.params)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}
export const getAllProduct = async (req, res) => {

    try {
        const response = await productService.getAllProductService(req.params)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}
export const updateProduct = async (req, res) => {

    try {
        const response = await productService.updateProductService(req.file ? { ...req.body, product_img: req.file.filename } : req.body)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}
export const deleteProduct = async (req, res) => {
    const { id } = req.params

    try {
        const response = await productService.deleteProductService(id)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at product controller: ' + error })

    }
}



