import * as userService from '../services/userService'

export const getCurrentUser = async (req, res) => {
    const { id } = req.user
    try {
        const response = await userService.getCurrentUserService(id)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at auth controller: ' + error })

    }
}
export const getAllUser = async (req, res) => {
    try {
        const response = await userService.getAllUserService()
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at auth controller: ' + error })

    }
}
export const editUser = async (req, res) => {
    const { id } = req.user
    const data = req.body
    try {
        const response = await userService.editUserService({ ...data, id })
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at auth controller: ' + error })

    }
}



