import * as orderService from '../services/orderService'

export const order = async (req, res) => {

    const { id } = req.user
    const dataCart = req.body


    try {
        const response = await orderService.orderService(id, dataCart)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const getAllOrder = async (req, res) => {



    try {
        const response = await orderService.getAllOrderService()
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const getOrderLimit = async (req, res) => {
    const { limit } = req.params



    try {
        const response = await orderService.getOrderLimitService(limit)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const getOrderUser = async (req, res) => {



    try {
        const response = await orderService.getOrderUserService(req.params)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const getOrderDetails = async (req, res) => {
    const { id } = req.params


    try {
        const response = await orderService.getOrderDetailsService(id)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const deleteOrderDetails = async (req, res) => {
    const { id } = req.params


    try {
        const response = await orderService.deleteOrderDetailsService(id)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}
export const editStatusOrder = async (req, res) => {


    try {
        const response = await orderService.editStatusOrderService(req.body)
        return res.status(200).json(response)

    } catch (error) {
        return res.status(500).json({ err: -1, msg: 'Fail at order controller: ' + error })

    }
}




