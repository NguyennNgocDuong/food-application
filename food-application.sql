-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2023 at 08:59 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food-application`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `createdAt`, `updatedAt`) VALUES
(4, 'Fish', '2023-06-13 15:06:50', '2023-06-13 15:06:50'),
(5, 'Chicken', '2023-06-13 15:28:07', '2023-06-15 17:21:44'),
(7, 'Fruit', '2023-06-13 15:42:50', '2023-06-13 16:11:45'),
(8, 'Curry', '2023-06-15 03:22:53', '2023-06-15 03:22:53'),
(9, 'Icecreams', '2023-06-15 17:21:03', '2023-06-15 17:21:03'),
(10, 'Drink', '2023-06-15 17:21:38', '2023-06-15 17:21:38');

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `order_id`, `product_id`, `quantity`, `createdAt`, `updatedAt`) VALUES
(68, 54, 7, 1, '2023-06-14 04:03:40', '2023-06-14 04:03:40'),
(69, 54, 2, 1, '2023-06-14 04:03:40', '2023-06-14 04:03:40'),
(70, 54, 8, 1, '2023-06-14 04:03:40', '2023-06-14 04:03:40'),
(71, 55, 2, 1, '2023-06-14 04:18:11', '2023-06-14 04:18:11'),
(72, 55, 3, 1, '2023-06-14 04:18:11', '2023-06-14 04:18:11'),
(73, 56, 7, 1, '2023-06-14 15:56:00', '2023-06-14 15:56:00'),
(74, 56, 6, 1, '2023-06-14 15:56:00', '2023-06-14 15:56:00'),
(75, 57, 1, 1, '2023-06-14 16:06:42', '2023-06-14 16:06:42'),
(76, 57, 2, 1, '2023-06-14 16:06:42', '2023-06-14 16:06:42'),
(77, 58, 1, 1, '2023-06-15 02:49:23', '2023-06-15 02:49:23'),
(78, 58, 2, 1, '2023-06-15 02:49:23', '2023-06-15 02:49:23'),
(79, 59, 2, 1, '2023-06-15 02:55:51', '2023-06-15 02:55:51'),
(80, 59, 3, 1, '2023-06-15 02:55:51', '2023-06-15 02:55:51'),
(81, 60, 1, 2, '2023-06-15 03:21:50', '2023-06-15 03:21:50'),
(82, 60, 2, 2, '2023-06-15 03:21:50', '2023-06-15 03:21:50'),
(83, 60, 7, 2, '2023-06-15 03:21:50', '2023-06-15 03:21:50'),
(84, 61, 6, 1, '2023-06-15 04:54:27', '2023-06-15 04:54:27'),
(85, 61, 7, 1, '2023-06-15 04:54:27', '2023-06-15 04:54:27'),
(86, 62, 10, 3, '2023-06-24 10:46:13', '2023-06-24 10:46:13'),
(87, 63, 10, 2, '2023-06-24 10:49:36', '2023-06-24 10:49:36');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `status` enum('processing','shipping','delieverd','cancel') DEFAULT 'processing',
  `total` int(11) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `createdAt`, `updatedAt`, `status`, `total`, `payment_method`, `address`) VALUES
(54, 3, '2023-06-14 04:03:40', '2023-06-14 15:58:40', 'delieverd', 1076, 'Cash on delivery', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(55, 3, '2023-06-14 04:18:11', '2023-06-14 16:00:36', 'delieverd', 655, 'Cash on delivery', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(56, 3, '2023-06-14 15:56:00', '2023-06-14 16:03:37', 'delieverd', 1849, 'Cash on delivery', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(57, 3, '2023-06-14 16:06:42', '2023-06-15 04:51:42', 'shipping', 982, 'Cash on delivery', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(58, 3, '2023-06-15 02:49:23', '2023-06-15 05:06:30', 'shipping', 982, 'paypal', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(59, 1, '2023-06-15 02:55:51', '2023-06-15 05:03:58', 'processing', 655, 'paypal', 'Aperiam facere et er ,   Tỉnh Bắc Kạn'),
(60, 8, '2023-06-15 03:21:50', '2023-06-15 03:24:03', 'delieverd', 4426, 'paypal', '57 Điên Biên Phủ , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(61, 8, '2023-06-15 04:54:26', '2023-06-15 05:01:20', 'processing', 1849, 'paypal', '57 Điên Biên Phủ , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh'),
(62, 10, '2023-06-24 10:46:13', '2023-06-24 10:46:13', 'processing', 369, 'Cash on delivery', ''),
(63, 10, '2023-06-24 10:49:35', '2023-06-24 10:52:50', 'delieverd', 246, 'paypal', 'Binh Loi , Phường Tây Tựu , Quận Bắc Từ Liêm , Thành phố Hà Nội');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_img` varchar(255) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `product_calories` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `product_img`, `product_price`, `product_calories`, `createdAt`, `updatedAt`) VALUES
(1, 5, 'Chancellor Delgadoo', 'image-1686711921718.c1.png', 913, 59, '2023-06-14 03:05:21', '2023-06-15 17:20:45'),
(2, 4, 'Michelle Dalton', 'image-1686712145489.fi3.png', 69, 54, '2023-06-14 03:09:05', '2023-06-14 03:09:05'),
(3, 9, 'Uriah Daniels', 'image-1686712364461.d7.png', 586, 30, '2023-06-14 03:12:44', '2023-06-15 17:21:10'),
(5, 8, 'Chase Garrett', 'image-1686712480792.cu4.png', 97, 100, '2023-06-14 03:14:40', '2023-06-15 17:21:21'),
(6, 5, 'Kevyn Wilson', 'image-1686712784623.c1.png', 618, 29, '2023-06-14 03:19:44', '2023-06-15 17:21:27'),
(7, 10, 'Coca', 'image-1686712820298.d8.png', 1231, 22, '2023-06-14 03:20:20', '2023-06-15 17:21:57'),
(10, 5, 'Phat Ga Qua', 'image-1687604058717.c4.png', 123, 121, '2023-06-24 10:43:59', '2023-06-24 10:54:18');

-- --------------------------------------------------------

--
-- Table structure for table `sequelizemeta`
--

CREATE TABLE `sequelizemeta` (
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sequelizemeta`
--

INSERT INTO `sequelizemeta` (`name`) VALUES
('migration-add-column-order.js'),
('migration-create-category.js'),
('migration-create-order.js'),
('migration-create-orderItem.js'),
('migration-create-product.js'),
('migration-create-user.js');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phoneNumber` int(11) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `firstName`, `lastName`, `email`, `password`, `address`, `phoneNumber`, `gender`, `image`, `createdAt`, `updatedAt`) VALUES
(1, 'user', 'Brandon', 'Bean', 'duong3@gmail.com', '$2a$10$lU2EJ.5aGUDKwJz8eKX6celQ7HujILlm86Zir2EI3kQveftPer5rG', 'Aperiam facere et er ,   Tỉnh Bắc Kạn', 12123123, NULL, NULL, '2023-05-29 11:44:09', '2023-05-30 04:19:01'),
(2, 'user', 'Duong', 'Nguyen', 'duong1@gmail.com', '$2a$10$PGlRoOO/qnBhG7.yQeAy7ObGs2xE84yV8jhCEKZoekwRAN7AWFghq', NULL, NULL, NULL, NULL, '2023-05-30 03:47:12', '2023-05-30 03:47:12'),
(3, 'user', 'DD', 'Holder', 'duong2@gmail.com', '$2a$10$pFjn/Dbo.IhJE0qjPPoBguEJsHxPCgsXdCYI32c7s85I/IGMlfWry', 'Dien Bien Phu , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh', 21321321, NULL, NULL, '2023-06-02 08:40:07', '2023-06-06 14:51:37'),
(4, 'user', 'Keane', 'Bernard', 'duong4@gmail.com', '$2a$10$eLU/mke7zY9jwHpVIRCzR.VDnuNonVAhJV0qn7y8VizNI3mdXzWBu', NULL, NULL, NULL, NULL, '2023-06-06 15:26:58', '2023-06-06 15:26:58'),
(5, 'admin', 'Duong', 'Nguyen', 'admin@gmail.com', '$2a$10$GgRp30Car/akWBHQ6IKnVeqcDHs7s9LP51qdDHhaTioXJ4N4qrjLy', NULL, NULL, NULL, NULL, '2023-06-06 15:46:44', '2023-06-06 15:46:44'),
(8, 'user', 'Duong', 'Nguyen', 'duong5@gmail.com', '$2a$10$iX2/v8QKGns9Wowf52UYZeeuf19o7KzwbfhSZkNmKhiPvRppufSgK', '57 Điên Biên Phủ , Phường 15 , Quận Bình Thạnh , Thành phố Hồ Chí Minh', 12345678, NULL, NULL, '2023-06-15 03:20:31', '2023-06-15 03:21:14'),
(9, 'user', 'Duong', 'Nguyen', 'duong6@gmail.com', '$2a$10$hmAZlBXMRlKUXgylEVO8geeouUIseEY1N5oUvfOVbOT8eYJG/opvW', NULL, NULL, NULL, NULL, '2023-06-15 16:52:59', '2023-06-15 16:52:59'),
(10, 'user', 'Phat', 'Nguyen', 'phat1@gmail.com', '$2a$10$6JambWWQOgrMmY8gE8HgSeNINVsXq8XSOvhDw8i2lMQZ4CxEZ1c02', 'Binh Loi , Phường Tây Tựu , Quận Bắc Từ Liêm , Thành phố Hà Nội', 123213213, NULL, NULL, '2023-06-24 10:42:04', '2023-06-24 10:45:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sequelizemeta`
--
ALTER TABLE `sequelizemeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
