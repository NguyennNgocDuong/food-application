import "./App.css";
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { routes } from "./routes/routes";
import { AnimatePresence } from "framer-motion";

import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import LoadingSpin from "./Components/LoadingSpin/LoadingSpin";



function App() {
  return (
    <BrowserRouter>
      <LoadingSpin />
      <AnimatePresence exitBeforeEnter>
        <ToastContainer />
        <div className="w-screen flex flex-col bg-primaryColor">

          <Routes>
            {routes.map((route, i) => {
              return <Route key={i} path={route.path} element={route.element} />
            })}
          </Routes>
        </div>
      </AnimatePresence>

    </BrowserRouter>
  );
}

export default App;
