import React, { memo, useEffect, useMemo, useState } from "react";

import {
  MdReorder,
  MdShoppingBasket,
  MdAdd,
  MdLogout,
  MdHome,
  MdRestaurantMenu,
  MdInfo,
  MdCall,
  MdPerson,
} from "react-icons/md";
import { motion } from "framer-motion";

import Logo from "../../img/logo.png";
import Avatar from "../../img/avatar.png";

import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { set_user } from "../../redux/slice/authSlice";
import { userLocal } from "../../services/localServices";
import Button from "../Button/Button";

const Header = () => {
  const user = useSelector((state) => state.authSlice.currentUser);
  const cart = useSelector((state) => state.cartSlice.listCarts);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isMenu, setIsMenu] = useState(false);

  // đăng xuất
  const logout = () => {
    setIsMenu(false);
    userLocal.remove();
    const action = set_user(null);
    dispatch(action);
    navigate("/login");
  };

  const getTotalQuantity = useMemo(() => {
    return cart.reduce((preVal, currentVal) => {
      return preVal + currentVal?.qty;
    }, 0);
  }, [cart]);

  return (
    <header className="fixed z-40 w-screen p-3 px-4 md:p-6 md:px-16 bg-orange-100">
      {/* desktop */}
      <div className="hidden md:flex w-full h-full items-center justify-between">
        <Link to="/" className="flex items-center gap-2 cursor-pointer">
          <img src={Logo} className="w-10 object-cover" alt="" />
          <p className="text-headingColor text-xl font-bold">HFY</p>
        </Link>

        <div className="relative flex items-center justify-center">
          <Link className="relative" to="/cart">
            <MdShoppingBasket className=" text-textColor text-2xl cursor-pointer" />
            <div className="absolute top-[-8px] right-[-11px] flex items-center justify-center w-5 h-5 rounded-full bg-cartNumBg ">
              <p className="text-sm text-white font-semibold ">
                {getTotalQuantity}
              </p>
            </div>
          </Link>

          {user ? (
            <div className="relative">
              <motion.img
                whileTap={{ scale: 0.6 }}
                className="w-8 h-8 min-w-[40px] min-h-[40px] ml-5 drop-shadow-xl cursor-pointer rounded-full"
                src={user?.photoURL ? user?.photoURL : Avatar}
                // src="https://lh3.googleusercontent.com/a/AEdFTp6qB1K0mf32xzQa9i0bfzTTNf-bnt97wDjPNm0O3Q=s96-c"
                alt="userprofile"
                onClick={() => setIsMenu(!isMenu)}
              />
              {isMenu && (
                <motion.div
                  initial={{ opacity: 0, scale: 0.6 }}
                  animate={{ opacity: 1, scale: 1 }}
                  exit={{ opacity: 0, scale: 0.6 }}
                  className="absolute right-0 top-12 flex flex-col w-48 bg-gray-50 shadow-xl rounded-lg "
                >
                  <Link
                    to="/profile"
                    className="px-4 py-2 flex items-center gap-3 cursor-pointer hover:bg-slate-100 transition-all duration-100 ease-in-out text-textColor text-base"
                  >
                    <MdPerson /> {user?.firstName}
                    {user?.lastName}
                  </Link>
                  <Link
                    to="/order"
                    className="px-4 py-2 flex items-center gap-3 cursor-pointer hover:bg-slate-100 transition-all duration-100 ease-in-out text-textColor text-base"
                  >
                    <MdReorder /> My order
                  </Link>
                  <p
                    className="px-4 py-2 flex items-center gap-3 cursor-pointer hover:bg-slate-100 transition-all duration-100 ease-in-out text-textColor text-base"
                    onClick={logout}
                  >
                    <MdLogout /> Log Out
                  </p>
                </motion.div>
              )}
            </div>
          ) : (
            <Link
              to="/login"
              className="ml-5 px-3 py-2 bg-white border border-black rounded-sm transition-all duration-100 ease-in-out hover:bg-orange-300 hover:text-white hover:border-orange-300"
            >
              Đăng nhập
            </Link>
          )}
        </div>
      </div>
    </header>
  );
};

export default memo(Header);
