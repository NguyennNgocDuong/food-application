import React from "react";

const MainContainer = ({ children }) => {
  return (
    <main className="mt-14 md:mt-20 px-4 md:px-16 py-4 w-full ">
      {children}
    </main>
  );
};

export default MainContainer;
