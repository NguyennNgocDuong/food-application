import React, { memo } from "react";
import { Divider, Radio, Table } from "antd";
import { useState } from "react";

const TableComponent = (props) => {
  const { selectionType = "checkbox", columns, data, footer } = props;
  const dataTable =
    data?.length &&
    data?.map((item) => {
      return { ...item, key: item.id };
    });

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User",
      // Column configuration not to be checked
      name: record.name,
    }),
  };
  return (
    <div>
      <Table
        className=" mt-5 shadow-2xl "
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        pagination={{
          hideOnSinglePage: true,
        }}
        columns={columns}
        dataSource={dataTable}
        footer={footer}
      />
    </div>
  );
};
export default memo(TableComponent);
