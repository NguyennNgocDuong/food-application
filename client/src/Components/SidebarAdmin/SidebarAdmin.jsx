import React, { memo, useState } from "react";
import { MdOutlineDashboard } from "react-icons/md";
import { TbReportAnalytics } from "react-icons/tb";
import { AiOutlineUser } from "react-icons/ai";
import { FiShoppingCart, FiChevronLeft } from "react-icons/fi";
import { BiFoodMenu } from "react-icons/bi";
import { Link } from "react-router-dom";

import Logo from "../../img/logo.png";

const SidebarAdmin = () => {
  const menus = [
    { name: "Dashboard", link: "/", icon: MdOutlineDashboard },
    { name: "User", link: "/admin/user-management", icon: AiOutlineUser },
    { name: "Food", link: "/admin/food-management", icon: BiFoodMenu },
    {
      name: "Categories",
      link: "/admin/category-management",
      icon: TbReportAnalytics,
      margin: true,
    },
    { name: "Order", link: "/admin/order-management", icon: FiShoppingCart },
  ];
  const [open, setOpen] = useState(true);

  return (
    <div
      className={`bg-orange-400 min-h-screen ${
        open ? "w-72" : "w-16"
      } duration-500 text-textColor-100 px-4 pt-8 relative`}
    >
      <div
        className={`absolute cursor-pointer -right-3 top-9 w-7 h-7 flex items-center justify-center border-black
          border-2 rounded-full bg-white  ${!open && "rotate-180"}`}
        onClick={() => setOpen(!open)}
      >
        <FiChevronLeft />
      </div>
      <Link
        to="/"
        className="flex items-center text-2xl  gap-3.5 font-semibold p-2 cursor-pointer transition-all ease-in-out duration-300 rounded-md"
      >
        <img
          width={30}
          src={Logo}
          className={`cursor-pointer duration-500 ${open && "rotate-[360deg]"}`}
        />
        <h1
          className={` origin-left font-medium text-xl duration-200 ${
            !open && "scale-0"
          }`}
        >
          HFY
        </h1>
      </Link>
      <div className="mt-4 flex flex-col gap-4 relative">
        {menus?.map((menu, i) => (
          <Link
            to={menu?.link}
            key={i}
            className="group flex items-center text-sm  gap-3.5 font-medium p-2 hover:bg-orange-300 transition-all ease-in-out duration-300 rounded-md"
          >
            <div>{React.createElement(menu?.icon, { size: "20" })}</div>
            <h2
              style={{
                transitionDelay: `${i + 3}00ms`,
              }}
              className={`whitespace-pre duration-500 ${
                !open && "opacity-0 translate-x-28 overflow-hidden"
              }`}
            >
              {menu?.name}
            </h2>
            <h2
              className={`${
                open && "hidden"
              } absolute z-50 left-48 bg-white font-semibold whitespace-pre text-gray-900 rounded-md drop-shadow-lg px-0 py-0 w-0 overflow-hidden group-hover:px-2 group-hover:py-1 group-hover:left-14 group-hover:duration-300 group-hover:w-fit  `}
            >
              {menu?.name}
            </h2>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default memo(SidebarAdmin);
