import { useFormik } from "formik";
import * as Yup from "yup";

import React, { memo, useEffect, useState } from "react";

import {
  MdFastfood,
  MdCloudUpload,
  MdDelete,
  MdFoodBank,
  MdAttachMoney,
} from "react-icons/md";

import { toast } from "react-toastify";
import { productService } from "../../services/productService";
import { categoryService } from "../../services/categoryService";
const FormAddFood = ({
  isOpenModal,
  data,
  onSetCurrentFoodSelected,
  onSetListFood,
  type,
}) => {
  const [imageAsset, setImageAsset] = useState(null);
  const [listCategories, setListCategories] = useState([]);
  const formik = useFormik({
    initialValues: {
      product_name: data ? data.product_name : "",
      category_id: data ? data.category_id : "",
      product_img: data ? data.product_img : imageAsset,
      product_calories: data ? data.product_calories : "",
      product_price: data ? data.product_price : "",
    },
    validationSchema: Yup.object({
      product_name: Yup.string().required("You must fill in this section!"),
      category_id: Yup.string().required("You must fill in this section!"),
      product_img: Yup.string().required("You must choose image!"),
      product_calories: Yup.number().required("You must fill in this section!"),
      product_price: Yup.number().required("You must fill in this section!"),
    }),
    enableReinitialize: true,
    onSubmit: async (values) => {
      isOpenModal(false);
      onSetCurrentFoodSelected(null);
      const formData = new FormData();
      formData.append("product_name", values.product_name);
      formData.append("category_id", values.category_id);
      formData.append("product_img", values.product_img);
      formData.append("product_calories", values.product_calories);
      formData.append("product_price", values.product_price);
      formData.append("id", data?.id);
      try {
        let res;
        if (type === "create") {
          res = await productService.handleAddProductApi(formData);
        } else {
          res = await productService.handleUpdateProductApi(formData);
        }
        if (res.data.err === 0) {
          toast.success(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          const listFood = await productService.handleGetAllProductApi();

          onSetListFood(listFood.data.response);
        } else {
          toast.error(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      } catch (error) {
        console.log("error: ", error);
      }
    },
  });
  const fetchCategoryApi = async () => {
    try {
      let res = await categoryService.handleGetAllCategoryApi();
      setListCategories(res.data.response);
    } catch (error) {
      console.log("error: ", error);
    }
  };
  useEffect(() => {
    data && setImageAsset(data.product_img);
    return () => {
      setImageAsset(null);
    };
  }, [data]);
  useEffect(() => {
    fetchCategoryApi();
  }, []);

  useEffect(() => {
    return () => {
      imageAsset && URL.revokeObjectURL(imageAsset.preview);
    };
  }, [imageAsset]);
  return (
    <div className="w-full mt-5">
      <div className="w-full flex justify-center">
        <div className="w-full my-3 text-xl text-gray-900 font-semibold">
          <form
            onSubmit={formik.handleSubmit}
            className="w-full  border border-gray-300 rounded-lg p-4 flex flex-col  gap-4"
          >
            <div className="w-full py-2 border-b border-gray-300 flex items-center gap-2">
              <MdFastfood className="text-xl text-gray-700" />
              <input
                name="product_name"
                onChange={formik.handleChange}
                value={formik.values.product_name}
                type="text"
                placeholder="Enter food name..."
                className="w-full h-full text-lg bg-transparent outline-none border-none placeholder:text-gray-400 text-textColor"
              />
            </div>
            {formik.errors.product_name && formik.touched.product_name && (
              <p className="text-red-500 text-sm italic">
                {formik.errors.product_name}
              </p>
            )}

            <div className="w-full">
              <select
                className="outline-none w-full text-base border-b-2 border-gray-200 p-2 rounded-md cursor-pointer"
                onChange={formik.handleChange}
                name="category_id"
                value={formik.values.category_id}
              >
                <option value="" className="bg-white">
                  Select Category
                </option>
                {listCategories.map((category) => (
                  <option
                    key={category.id}
                    className="text-base border-0 outline-none capitalize bg-white text-headingColor"
                    value={category.id}
                    // name="category_id"
                  >
                    {category.category_name}
                  </option>
                ))}
              </select>
              {formik.errors.category_id && formik.touched.category_id && (
                <p className="text-red-500 text-sm italic">
                  {formik.errors.category_id}
                </p>
              )}
            </div>
            <div className="group flex justify-center items-center flex-col border-2 border-dotted border-gray-300 w-full h-225 md:h-340 cursor-pointer rounded-lg">
              <>
                {!imageAsset ? (
                  <>
                    <label className="w-full h-full flex flex-col items-center justify-center cursor-pointer">
                      <div className="w-full h-full flex flex-col items-center justify-center gap-2">
                        <MdCloudUpload className="text-gray-500 text-3xl hover:text-gray-700" />
                        <p className="text-gray-500 hover:text-gray-700">
                          Click here to upload
                        </p>
                      </div>
                      <input
                        type="file"
                        name="product_img"
                        accept="image/*"
                        onChange={(e) => {
                          const file = e.target.files[0];
                          file.preview = URL.createObjectURL(file);

                          setImageAsset(file);

                          return formik.setFieldValue(
                            "product_img",
                            e.target.files[0]
                          );
                        }}
                        className="w-0 h-0"
                      />
                    </label>
                  </>
                ) : (
                  <>
                    <div className="relative h-full">
                      <img
                        src={
                          imageAsset?.preview
                            ? imageAsset.preview
                            : `http://localhost:8080/image/${imageAsset}`
                        }
                        alt="uploaded image"
                        className="w-full h-full object-cover"
                      />
                      <button
                        type="button"
                        className="absolute bottom-3 right-3 p-3 rounded-full bg-red-500 text-xl cursor-pointer outline-none hover:shadow-md  duration-500 transition-all ease-in-out"
                        onClick={() => setImageAsset(null)}
                      >
                        <MdDelete className="text-white" />
                      </button>
                    </div>
                  </>
                )}
              </>
            </div>
            {formik.errors.product_img && formik.touched.product_img && (
              <p className="text-red-500 text-sm italic">
                {formik.errors.product_img}
              </p>
            )}
            <div className="w-full flex flex-col md:flex-row items-center gap-3">
              <div className="w-full">
                <div className="w-full py-2 border-b border-gray-300 flex items-center gap-2">
                  <MdFoodBank className="text-gray-700 text-2xl" />
                  <input
                    name="product_calories"
                    onChange={formik.handleChange}
                    value={formik.values.product_calories}
                    type="number"
                    placeholder="Calories"
                    className="w-full h-full text-lg bg-transparent outline-none border-none placeholder:text-gray-400 text-textColor"
                  />
                </div>
                {formik.errors.product_calories &&
                  formik.touched.product_calories && (
                    <p className="text-red-500 text-sm italic">
                      {formik.errors.product_calories}
                    </p>
                  )}
              </div>

              <div className="w-full">
                <div className="w-full py-2 border-b border-gray-300 flex items-center gap-2">
                  <MdAttachMoney className="text-gray-700 text-2xl" />
                  <input
                    name="product_price"
                    onChange={formik.handleChange}
                    type="number"
                    value={formik.values.product_price}
                    placeholder="Price"
                    className="w-full h-full text-lg bg-transparent outline-none border-none placeholder:text-gray-400 text-textColor"
                  />
                </div>
                {formik.errors.product_price &&
                  formik.touched.product_price && (
                    <p className="text-red-500 text-sm italic">
                      {formik.errors.product_price}
                    </p>
                  )}
              </div>
            </div>
            <div className="flex items-center w-full">
              <button
                type="submit"
                className="ml-0 md:ml-auto w-full md:w-auto border-none outline-none bg-orange-400 px-12 py-2 rounded-lg text-lg text-white font-semibold hover:bg-orange-600 duration-100 transition-all ease-in-out "
              >
                Save
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default memo(FormAddFood);
