import { motion } from "framer-motion";
import React, { memo, useEffect, useRef } from "react";
import { MdShoppingBasket } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import NotFound from "../../img/NotFound.svg";
import { toast } from "react-toastify";
import { add_to_cart } from "../../redux/slice/cartSlice";

const ListFood = ({ flag, data }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const user = useSelector((state) => state.authSlice.currentUser);
  const refListFood = useRef();

  // add to cart
  const handleAddToCart = (item) => {
    if (user) {
      const action = add_to_cart({ ...item, uid: user.id });
      dispatch(action);
    } else {
      navigate("/login");

      toast.error("Please Sign in!", {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  return (
    <>
      {data && data.length ? (
        <div ref={refListFood} className="w-full grid grid-cols-4 my-12 gap-5">
          {data.map((item) => (
            <div
              key={item?.id}
              className="w-280 h-[175px] min-w-[280px] md:w-full md:min-w-[300px]  bg-cardOverlay rounded-lg py-2 px-4   backdrop-blur-lg hover:drop-shadow-lg flex flex-col items-center justify-evenly relative"
            >
              <div className="w-full flex items-center justify-between">
                <motion.div
                  className="w-40 h-40 -mt-8 drop-shadow-2xl"
                  whileHover={{ scale: 1.2 }}
                >
                  <img
                    src={`http://localhost:8080/image/${item.product_img}`}
                    alt="food"
                    className="w-full h-full object-contain"
                  />
                </motion.div>
                <motion.div
                  whileTap={{ scale: 0.75 }}
                  className="w-8 h-8 rounded-full bg-red-600 flex items-center justify-center cursor-pointer hover:shadow-md -mt-8"
                  onClick={() => handleAddToCart(item)}
                >
                  <MdShoppingBasket className="text-white" />
                </motion.div>
              </div>

              <div className="w-full flex flex-col items-end justify-end -mt-8">
                <p className="text-textColor font-semibold text-base md:text-lg">
                  {item?.product_name}
                </p>
                <p className="mt-1 text-sm text-gray-500">
                  {item?.product_calories} Calories
                </p>
                <div className="flex items-center gap-8">
                  <p className="text-lg text-headingColor font-semibold">
                    <span className="text-sm text-red-500">$</span>{" "}
                    {item?.product_price}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      ) : (
        <div className="w-full flex flex-col items-center justify-center">
          <img src={NotFound} className="h-340" />
          <p className="text-xl text-headingColor font-semibold my-2">
            Food Not Available
          </p>
        </div>
      )}
    </>
  );
};

export default memo(ListFood);
