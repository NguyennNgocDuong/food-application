import React, { memo } from "react";

const Input = (props) => {
  return (
    <div className={props.label ? `flex flex-col mt-5 ${props.className}` : ""}>
      {props.label && (
        <label htmlFor={props.label} className="font-medium text-gray-500">
          {props.label}
        </label>
      )}
      <input
        id={props.label}
        value={props.value}
        disabled={props.disabled}
        className="px-3 py-1 border border-gray-300 rounded mt-1 focus:outline-orange-400"
        {...props}
      />
    </div>
  );
};

export default memo(Input);
