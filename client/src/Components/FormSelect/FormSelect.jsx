import React, { memo } from "react";

const FormSelect = ({
  label,
  options,
  value,
  setValue,
  type,
  className,
  isLabel = true,
}) => {
  return (
    <div className={isLabel ? `flex flex-col mt-5 ${className}` : ""}>
      {isLabel && (
        <label htmlFor={label} className="font-medium text-gray-500">
          {label}
        </label>
      )}
      <select
        className={className}
        value={value}
        onChange={(e) => {
          if (e.target.value) {
            setValue({
              id: e.target.value,
              name: e.target[e.target.selectedIndex].text,
            });
          } else {
            setValue({
              id: e.target.value,
              name: e.target.value,
            });
          }
        }}
        name={label}
        id={label}
      >
        <option value="">Choose {label}</option>
        {type === "province" &&
          options?.map((option) => {
            return (
              <option key={option.province_id} value={option.province_id}>
                {option.province_name}
              </option>
            );
          })}
        {type === "district" &&
          options?.map((option) => {
            return (
              <option key={option.district_id} value={option.district_id}>
                {option.district_name}
              </option>
            );
          })}
        {type === "ward" &&
          options?.map((option) => {
            return (
              <option key={option.ward_id} value={option.ward_id}>
                {option.ward_name}
              </option>
            );
          })}
        {type === "category" &&
          options?.map((option) => {
            return (
              <option key={option.id} value={option.id}>
                {option.category_name}
              </option>
            );
          })}
        {type === "status" &&
          options?.map((option) => {
            return (
              <option key={option} value={option}>
                {option}
              </option>
            );
          })}
      </select>
    </div>
  );
};

export default memo(FormSelect);
