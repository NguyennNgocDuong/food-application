import React, { memo, useState } from "react";
import { Input } from "antd";
import Avatar from "../../img/avatar.png";
import { MdLogout } from "react-icons/md";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userLocal } from "../../services/localServices";
import { set_user } from "../../redux/slice/authSlice";

const { Search } = Input;
const HeaderAdmin = ({ onSetValueSearch }) => {
  const [openHeader, setOpenHeader] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const logout = () => {
    userLocal.remove();
    const action = set_user(null);
    dispatch(action);
    navigate("/login");
  };
  return (
    <div className="w-full bg-white px-8 py-3 flex items-center justify-between shadow-md">
      <Search
        onChange={(e) => onSetValueSearch(e.target.value)}
        allowClear
        placeholder="Search here...."
        style={{
          width: "50%",
        }}
      />

      <img
        onClick={() => setOpenHeader(!openHeader)}
        className="relative cursor-pointer  transition-all ease-in-out duration-500 "
        width={40}
        src={Avatar}
        alt=""
      />

      {openHeader && (
        <div className="z-50 w-[200px] absolute transition-all ease-in-out duration-500 border  right-[13px] top-[72px] bg-white rounded-lg shadow-lg before:absolute before:w-3 before:h-3 before:-top-1 before:right-3 before:rotate-45 before:bg-white">
          <div className="">
            <p
              className="flex items-center cursor-pointer text-sm  gap-3.5 font-medium p-3 my-3 hover:bg-gray-300 transition-all ease-in-out duration-300"
              onClick={logout}
            >
              <MdLogout style={{ fontSize: "20px" }} /> Log Out
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default memo(HeaderAdmin);
