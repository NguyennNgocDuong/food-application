import React, { memo } from "react";

const Button = (props) => {
  return (
    <button
      type={props.type}
      className={`py-2 rounded-md transition-all ease-in-out duration-300 ${props.className}`}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
};

export default memo(Button);
