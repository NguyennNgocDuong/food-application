import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import bg_loading from "../../utils/hN6ZMDgfqL.json";

const LoadingSpin = () => {
  const isLoading = useSelector((state) => state.loadingSlice.isLoading);
  return (
    isLoading && (
      <div className="fixed top-0 left-0 w-screen h-screen z-50 bg-gray-500/70">
        <div className=" scale-50 flex w-full h-full items-center justify-center m-auto">
          <Lottie animationData={bg_loading} />
        </div>
      </div>
    )
  );
};

export default LoadingSpin;
