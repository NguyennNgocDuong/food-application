import AdminPage from "../pages/AdminPage/AdminPage";
import CategoryManagement from "../pages/AdminPage/CategoryManagement";
import FoodManagement from "../pages/AdminPage/FoodManagement";
import OrderManagement from "../pages/AdminPage/OrderManagement/OrderManagement";
import UserManagement from "../pages/AdminPage/UserManagement.jsx/UserManagement";
import CartPage from "../pages/CartPage/CartPage";
import HomePage from "../pages/HomePage/HomePage";
import LoginPage from "../pages/LoginPage/LoginPage";
import MenuPage from "../pages/MenuPage/MenuPage";
import OrderPage from "../pages/OrderPage/OrderPage";
import ProfilePage from "../pages/ProfilePage/ProfilePage";
import SignUpPage from "../pages/SignUpPage/SignUpPage";

export const routes = [
    {
        path: '/',
        element: <HomePage />
    },

    // {
    //     path: '/menu',
    //     element: <MenuPage />
    // },
    {
        path: '/login',
        element: <LoginPage />
    },
    {
        path: '/sign-up',
        element: <SignUpPage />
    },
    {
        path: '/cart',
        element: <CartPage />
    },
    {
        path: '/profile',
        element: <ProfilePage />
    },
    {
        path: '/order',
        element: <OrderPage />
    },
    {
        path: '/admin',
        element: <AdminPage />
    },
    {
        path: '/admin/food-management',
        element: <FoodManagement />
    },
    {
        path: '/admin/category-management',
        element: <CategoryManagement />
    },
    {
        path: '/admin/user-management',
        element: <UserManagement />
    },
    {
        path: '/admin/order-management',
        element: <OrderManagement />
    },

]