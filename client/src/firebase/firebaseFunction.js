import {
    collection,
    doc,
    getDocs,
    orderBy,
    query,
    setDoc,
    updateDoc, increment,
    deleteDoc
} from "firebase/firestore";
import { firestore } from "./firebaseConfig";


// Saving  user
export const saveUser = async (data, uid) => {
    // t2 : collection name
    // t3 : id
    await setDoc(doc(firestore, "Users", `${uid}`), data, {
        merge: true,
    });
};

// get user
export const getAllUsers = async () => {
    const items = await getDocs(

        query(collection(firestore, "Users"))
    );

    return items.docs.map((doc) => doc.data());
};



// Saving new Item
export const saveItem = async (data) => {
    // t2 : collection name
    // t3 : id
    await setDoc(doc(firestore, "foodItems", `${Date.now()}`), data, {
        merge: true,
    });
};

// getall food items
export const getAllFoodItems = async () => {
    const items = await getDocs(

        query(collection(firestore, "foodItems"), orderBy("id", "desc"))
    );

    return items.docs.map((doc) => doc.data());
};

// Saving new cart user
export const setCartUser = async (data, uid) => {
    // t2 : collection name
    // t3 : id
    await setDoc(doc(firestore, `Cart ${uid}`, data.id), data, {
        merge: true,
    });
};

// update cart user
export const updateCartUser = async (id, uid, number, currentPrice, currentQty) => {
    // t2 : collection name
    // t3 : id
    await updateDoc(doc(firestore, `Cart ${uid}`, id), {
        qty: increment(number),

    });
};

export const deleteCartUser = async (id, uid) => {
    // t2 : collection name
    // t3 : id
    await deleteDoc(doc(firestore, `Cart ${uid}`, id))
};

export const getCartUser = async (uid) => {
    const items = await getDocs(

        query(collection(firestore, `Cart ${uid}`), orderBy("id", "desc"))
    );

    return items.docs.map((doc) => doc.data());
};



