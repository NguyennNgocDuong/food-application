import { initializeApp, getApp, getApps } from "firebase/app";
import { getFirestore } from 'firebase/firestore'
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyBCW9ZqO4cJUemeakixSA4HHkZSFXlum4s",
    authDomain: "foodweb-86924.firebaseapp.com",
    databaseURL: "https://foodweb-86924-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "foodweb-86924",
    storageBucket: "foodweb-86924.appspot.com",
    messagingSenderId: "423333172179",
    appId: "1:423333172179:web:f232470b6be783fe3a9526"
};

// Initialize Firebase
const app = getApps.length > 0 ? getApp() : initializeApp(firebaseConfig);

const firestore = getFirestore(app)
const storage = getStorage(app)


export { app, firestore, storage }