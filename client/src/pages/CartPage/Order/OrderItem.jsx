import React from "react";

import { AiFillUpCircle, AiFillDownCircle } from "react-icons/ai";
import { BiTrash } from "react-icons/bi";
import { motion } from "framer-motion";

// antd
import { Button, Modal, Space } from "antd";
import { ExclamationCircleFilled } from "@ant-design/icons";
const { confirm } = Modal;

const OrderItem = ({ itemCart, onChangeQuantity, onRemoveCart }) => {
  const { product_calories, product_img, product_price, product_name, qty } =
    itemCart;

  const showDeleteConfirm = () => {
    confirm({
      title: "Are you sure delete this food?",
      icon: <ExclamationCircleFilled />,
      content: "",
      okText: "Yes",
      okType: "danger",
      cancelText: "No",
      onOk() {
        onRemoveCart(itemCart.id);
      },
      onCancel() {},
    });
  };
  return (
    <tr className="h-20">
      <td className="">
        <img
          width={75}
          height={75}
          src={`http://localhost:8080/image/${product_img}`}
          alt=""
        />
      </td>
      <td className="">
        <p className="text-textColor font-semibold text-base">{product_name}</p>
        <p className="text-textColor text-sm">{product_calories} Calories</p>
      </td>
      <td className="">
        <p className="flex items-center">{product_price}$</p>
      </td>
      <td className="">
        <div className="flex items-center">
          <motion.div
            whileTap={{ scale: 0.75 }}
            onClick={() => onChangeQuantity(-1, itemCart.id)}
            className="cursor-pointer"
          >
            <AiFillDownCircle className="text-xl" />
          </motion.div>
          <span className="mx-1"> {qty}</span>
          <motion.div
            whileTap={{ scale: 0.75 }}
            onClick={() => onChangeQuantity(1, itemCart.id)}
            className="cursor-pointer"
          >
            <AiFillUpCircle className="text-xl" />
          </motion.div>
        </div>
      </td>
      <td className="">{product_price * qty}$</td>
      <td>
        <Space wrap>
          <Button onClick={showDeleteConfirm}>
            <BiTrash className="text-xl cursor-pointer" />
          </Button>
        </Space>
      </td>
    </tr>
  );
};

export default OrderItem;
