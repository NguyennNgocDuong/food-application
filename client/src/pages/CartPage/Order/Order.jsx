import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { change_quantity, remove_cart } from "../../../redux/slice/cartSlice";
import Lottie from "lottie-react";
import empty_cart from "../../../utils/empty-cart.json";
import Button from "../../../Components/Button/Button";
import { Link } from "react-router-dom";
import OrderItem from "./OrderItem";

const Order = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.authSlice.currentUser);
  const dataCart = useSelector((state) => state.cartSlice.listCarts);

  const handleChangeQuantity = (number, id) => {
    const action = change_quantity({ id, uid: user.id, number });
    dispatch(action);
  };

  const handleRemoveCart = (id) => {
    const action = remove_cart({ id, uid: user.id });
    dispatch(action);
  };

  return (
    <div className="col-span-2 bg-white p-5 rounded-lg shadow-lg">
      <p className="text-2xl font-semibold relative after:absolute after:rounded-lg after:content after:w-full after:h-0.5 after:-bottom-2 after:left-0 after:bg-gradient-to-tr from-orange-400 to-orange-600 transition-all ease-in-out duration-100 ">
        My Cart
      </p>

      {dataCart?.length > 0 ? (
        <table className=" w-full mt-5">
          <thead>
            <tr>
              <th className="text-start">Image</th>
              <th className="text-start">Name</th>
              <th className="text-start">Price</th>
              <th className="text-start">Quantity</th>
              <th className="text-start">Total Price</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {dataCart.map((itemCart) => {
              return (
                <OrderItem
                  key={itemCart.id}
                  itemCart={itemCart}
                  onChangeQuantity={handleChangeQuantity}
                  onRemoveCart={handleRemoveCart}
                />
              );
            })}
          </tbody>
        </table>
      ) : (
        <div className=" w-full flex flex-col items-center justify-center">
          <Lottie style={{ width: 300 }} animationData={empty_cart} />
          <p className="text-xl font-semibold text-textColor">
            Opps...Your cart is empty!
          </p>
          <Button className="bg-orange-400 px-3 mt-3">
            <Link to="/">Order now</Link>
          </Button>
        </div>
      )}
    </div>
  );
};

export default Order;
