import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Button from "../../../Components/Button/Button";
import { toast } from "react-toastify";
import { orderService } from "../../../services/orderService";
import { cartLocal } from "../../../services/localServices";
import { Radio, Space } from "antd";
import { PayPalButton } from "react-paypal-button-v2";
import { paymentService } from "../../../services/paymentService";
import { clear_cart } from "../../../redux/slice/cartSlice";
import { FiSave } from "react-icons/fi";

const Payment = () => {
  const cart = useSelector((state) => state.cartSlice.listCarts);
  const user = useSelector((state) => state.authSlice.currentUser);
  const [total, setTotal] = useState(0);
  const [delivery, setDelivery] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [totalPaid, setTotalPaid] = useState(0);
  const [valuePaymentMethod, setValuePaymentMethod] =
    useState("Cash on delivery");
  const [isSdkPaypal, setIsSdkPaypal] = useState(false);
  console.log("isSdkPaypal: ", isSdkPaypal);
  const [valueInpAddress, setValueInpAddress] = useState(user.address);
  const [showInpAddress, setShowInpAddress] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const getTotalPrice = useMemo(() => {
    const total = cart.reduce((preVal, currentVal) => {
      return preVal + currentVal.product_price * currentVal.qty;
    }, 0);
    setTotal(total);
  }, [cart]);

  useEffect(() => {
    setTotalPaid(total + delivery - discount);
  }, [cart]);

  const fetchApiAddOrder = async () => {
    const res = await orderService.handleOrderApi({
      cart,
      totalPaid,
      valuePaymentMethod,
      address: valueInpAddress,
    });
    if (res.data.err === 0) {
      toast.success(res.data.msg, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      const action = clear_cart();
      dispatch(action);
    }
  };

  const checkout = async () => {
    if (cart.length > 0) {
      if (user.address && user.phoneNumber) {
        await fetchApiAddOrder();
      } else {
        toast.error("Please field address and phone number!!", {
          position: "top-right",
          autoClose: 1000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        navigate("/profile");
      }
    } else {
      toast.error("Please add food to cart!!", {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    navigate("/");
    }
  };
  const handleChangePaymentMethod = (e) => {
    setValuePaymentMethod(e.target.value);
  };

  const onSuccessPaypal = async (details, data) => {
    await fetchApiAddOrder();
  };
  const addPaypalScript = async () => {
    const { data } = await paymentService.handleGetClientIdPaypalApi();
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src = `https://www.paypal.com/sdk/js?client-id=${data.data}`;
    script.async = true;
    script.onload = () => {
      setIsSdkPaypal(true);
    };
    document.body.appendChild(script);
  };

  useEffect(() => {
    if (!window.paypal) {
      console.log("add paypal");
      addPaypalScript();
    } else {
      console.log("else");
      setIsSdkPaypal(true);
    }
  }, []);

  return (
    <div>
      <p className="font-semibold text-xl mb-3">Payment</p>
      <div className=" bg-white p-5 rounded-lg shadow-lg">
        <div className="grid grid-cols-2 gap-2 relative after:absolute after:rounded-lg after:content after:w-full after:h-0.5 after:-bottom-2 after:left-0 after:bg-gradient-to-tr from-orange-400 to-orange-600 transition-all ease-in-out duration-100 ">
          {user?.address ? (
            <p className="col-span-2">
              Ship to{" "}
              {showInpAddress ? (
                <div className="flex items-center">
                  <input
                    className="border border-gray-300 rounded mt-1 focus:outline-orange-400 px-2 w-full"
                    type="text"
                    onChange={(e) => setValueInpAddress(e.target.value)}
                    value={valueInpAddress}
                    placeholder="Enter address"
                  />

                  <button
                    className="flex items-center bg-orange-400 text-white px-2 py-1 rounded-md ml-3 hover:bg-orange-500 transition-all duration-100 ease-in-out"
                    onClick={() => setShowInpAddress(false)}
                  >
                    <FiSave />
                  </button>
                </div>
              ) : (
                <>
                  <span className="font-semibold ">{valueInpAddress}</span>
                  <button
                    onClick={() => setShowInpAddress(true)}
                    className="italic underline  text-orange-400 mx-1"
                  >
                    Thay đổi
                  </button>
                </>
              )}
            </p>
          ) : (
            <Button
              onClick={() => navigate("/profile")}
              className="col-span-2 w-full bg-red-400 text-white my-5 hover:bg-red-500 transition-all duration-100 ease-in-out"
            >
              Update address
            </Button>
          )}
          <p>Total</p>
          <p className="text-end">${total}</p>
          <p>Delivery</p>
          <p className="text-end">${delivery}</p>
          <p>Discount</p>
          <p className="text-end">${discount}</p>
        </div>
        <div className="grid grid-cols-2 mt-5 font-semibold">
          <p>Paid</p>
          <p className="text-end">${totalPaid}</p>
        </div>
        <Radio.Group
          className="font-semibold mt-4 "
          buttonStyle="solid"
          onChange={handleChangePaymentMethod}
          value={valuePaymentMethod}
        >
          <Space direction="vertical">
            <Radio value="Cash on delivery">Cash on delivery</Radio>
            <Radio value="paypal">Payment with Paypal</Radio>
          </Space>
        </Radio.Group>
        {valuePaymentMethod === "Cash on delivery" && (
          <Button
            onClick={checkout}
            className="w-full bg-orange-400 text-white my-5 hover:bg-orange-500 transition-all duration-100 ease-in-out"
          >
            Payment
          </Button>
        )}
        {valuePaymentMethod === "paypal" && (
          <div className="mt-5">
            <PayPalButton
              amount={totalPaid}
              // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
              onSuccess={onSuccessPaypal}
              onError={() => {
                alert("Erroe");
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default Payment;
