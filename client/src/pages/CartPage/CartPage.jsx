import React from "react";
import Header from "../../Components/Header/Header";
import MainContainer from "../../Components/MainContainer/MainContainer";
import Order from "./Order/Order";
import Payment from "./Payment/Payment";

const CartPage = () => {
  return (
    <>
      <Header />
      <MainContainer>
        <section className="w-full my-6">
          <div className="grid grid-cols-3 gap-5">
            <Order />
            <Payment />
          </div>
        </section>
      </MainContainer>
    </>
  );
};

export default CartPage;
