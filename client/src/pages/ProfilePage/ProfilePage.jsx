import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FormSelect from "../../Components/FormSelect/FormSelect";
import Header from "../../Components/Header/Header";
import Input from "../../Components/Input/Input";
import MainContainer from "../../Components/MainContainer/MainContainer";
import { toast } from "react-toastify";

import {
  getApiDistrictPublic,
  getApiProvincePublic,
  getApiWardPublic,
} from "../../services/app";
import { useFormik } from "formik";
import * as Yup from "yup";
import { userService } from "../../services/userService";
import { set_user } from "../../redux/slice/authSlice";
const ProfilePage = () => {
  const user = useSelector((state) => state.authSlice.currentUser);
  const dispatch = useDispatch();

  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const [provinceId, setPovinceId] = useState({});
  const [districtId, setDistrictId] = useState({});
  const [wardId, setWardId] = useState({});

  const fetchProvincePublic = async () => {
    const response = await getApiProvincePublic();
    if (response.status === 200) {
      setProvinces(response.data.results);
    }
  };
  const fetchDistrictPublic = async (provinceId) => {
    const response = await getApiDistrictPublic(provinceId);
    if (response.status === 200) {
      setDistricts(response.data.results);
    }
  };
  const fetchWardPublic = async (districtId) => {
    const response = await getApiWardPublic(districtId);
    if (response.status === 200) {
      setWards(response.data.results);
    }
  };
  const formik = useFormik({
    initialValues: {
      firstName: user.firstName,
      lastName: user.lastName,
      phoneNumber: user.phoneNumber ? user.phoneNumber : "",
      street: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string().required("You must fill in this section!"),
      lastName: Yup.string().required("You must fill in this section!"),
      phoneNumber: Yup.string().required("You must fill in this section!"),
      street: Yup.string().required("You must fill in this section!"),
    }),
    onSubmit: async (values) => {
      const dataUpdate = {
        ...values,
        address: `${formik.values.street ? `${formik.values.street} ,` : ""} ${
          wardId?.id ? `${wardId?.name} ,` : ""
        } ${districtId?.id ? `${districtId?.name} ,` : ""} ${
          provinceId.id ? provinceId.name : ""
        }`,
      };
      try {
        const res = await userService.handleUpdateUserInfoApi(dataUpdate);
        console.log("res: ", res);
        if (res.data.err === 0) {
          dispatch(
            set_user({
              ...user,
              firstName: values.firstName,
              lastName: values.lastName,
              phoneNumber: values.phoneNumber,
              address: dataUpdate.address,
            })
          );
          toast.success(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      } catch (error) {
        console.log("error: ", error);
      }
    },
  });

  useEffect(() => {
    fetchProvincePublic();
  }, []);

  useEffect(() => {
    setWards(null);
    setDistrictId(null);
    setWardId(null);
    setDistricts(null);

    if (provinceId.id) {
      fetchDistrictPublic(provinceId.id);
    }
  }, [provinceId]);
  useEffect(() => {
    if (districtId?.id) {
      fetchWardPublic(districtId?.id);
    }
  }, [districtId]);

  return (
    // w-[95%]
    <>
      <Header />
      <MainContainer>
        <div className="w-[75%] flex mx-auto">
          {/* <div className="w-[50%]">Hello</div> */}
          <div className="w-[100%]">
            <form onSubmit={formik.handleSubmit}>
              <div className="flex justify-between">
                <Input
                  name="firstName"
                  onChange={formik.handleChange}
                  value={formik.values.firstName}
                  label="First Name"
                  type="text"
                />
                {formik.errors.firstName && formik.touched.firstName && (
                  <p className="text-red-500 text-sm italic">
                    {formik.errors.firstName}
                  </p>
                )}
                <Input
                  name="lastName"
                  onChange={formik.handleChange}
                  value={formik.values.lastName}
                  label="Last Name"
                  type="text"
                />
                {formik.errors.lastName && formik.touched.lastName && (
                  <p className="text-red-500 text-sm italic">
                    {formik.errors.lastName}
                  </p>
                )}
              </div>
              <Input
                disabled={true}
                value={user.email}
                label="Email"
                type="email"
              />
              <Input
                name="phoneNumber"
                label="Phone Number"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.phoneNumber}
              />
              {formik.errors.phoneNumber && formik.touched.phoneNumber && (
                <p className="text-red-500 text-sm italic">
                  {formik.errors.phoneNumber}
                </p>
              )}

              <Input
                name="street"
                label="Street"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.street}
              />
              {formik.errors.street && formik.touched.street && (
                <p className="text-red-500 text-sm italic">
                  {formik.errors.street}
                </p>
              )}
              <FormSelect
                name="province"
                type="province"
                value={provinceId.id}
                setValue={setPovinceId}
                options={provinces}
                label="Province"
              />
              {formik.errors.province && formik.touched.province && (
                <p className="text-red-500 text-sm italic">
                  {formik.errors.province}
                </p>
              )}
              <div className="flex justify-between">
                <div className="w-[45%]">
                  <FormSelect
                    name="district"
                    type="district"
                    className="w-full"
                    value={districtId?.id}
                    setValue={setDistrictId}
                    options={districts}
                    label="District"
                  />
                  {formik.errors.district && formik.touched.district && (
                    <p className="text-red-500 text-sm italic">
                      {formik.errors.district}
                    </p>
                  )}
                </div>
                <div className="w-[45%]">
                  <FormSelect
                    name="ward"
                    type="ward"
                    className="w-full"
                    value={wardId?.id}
                    setValue={setWardId}
                    options={wards}
                    label="Ward"
                  />
                  {formik.errors.ward && formik.touched.ward && (
                    <p className="text-red-500 text-sm italic">
                      {formik.errors.ward}
                    </p>
                  )}
                </div>
              </div>
              <Input
                name="address"
                value={`${
                  formik.values.street ? `${formik.values.street} ,` : ""
                } ${wardId?.id ? `${wardId?.name} ,` : ""} ${
                  districtId?.id ? `${districtId?.name} ,` : ""
                } ${provinceId.id ? provinceId.name : ""}`}
                disabled={true}
                label="Full Address"
                type="text"
              />
              <button
                type="submit"
                className="flex mt-5 md:ml-auto w-full md:w-auto border-none outline-none bg-orange-400 px-12 py-2 rounded-lg text-lg text-white font-semibold hover:bg-orange-600 duration-100 transition-all ease-in-out "
              >
                Save
              </button>
            </form>
          </div>
        </div>
      </MainContainer>
    </>
  );
};

export default ProfilePage;
