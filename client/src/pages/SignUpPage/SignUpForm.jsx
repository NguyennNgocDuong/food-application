import React from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import Button from "../../Components/Button/Button";
import Input from "../../Components/Input/Input";
import { useFormik } from "formik";
import * as Yup from "yup";
import { userService } from "../../services/userService";

const SignUpForm = () => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()

        .max(11, "Your first name must be under 6 characters!")
        .required("You must fill in this section!"),
      lastName: Yup.string()

        .max(11, "Your last name must be under 6 characters!")
        .required("You must fill in this section!"),
      email: Yup.string()
        .email("Invalid Email")
        .required("You must fill in this section!"),
      password: Yup.string()
        .min(8, "Your password must be at least 8 characters!")
        .required("You must fill in this section!"),
    }),
    onSubmit: async (values) => {
      try {
        const res = await userService.handleSignUpApi({
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          password: values.password,
        });
        if (res.data.err === 0) {
          toast.success(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          navigate("/login");
        } else {
          toast.error(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      } catch (error) {
        console.log("error: ", error);
      }
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Input
        onChange={formik.handleChange}
        value={formik.values.firstName}
        type="text"
        label="First Name"
        name="firstName"
        placeholder="Enter your first name..."
      />
      {formik.errors.firstName && formik.touched.firstName && (
        <p className="text-red-500 text-sm italic">{formik.errors.firstName}</p>
      )}
      <Input
        onChange={formik.handleChange}
        value={formik.values.lastName}
        type="text"
        label="Last Name"
        name="lastName"
        placeholder="Enter your last name..."
      />
      {formik.errors.lastName && formik.touched.lastName && (
        <p className="text-red-500 text-sm italic">{formik.errors.lastName}</p>
      )}

      <Input
        onChange={formik.handleChange}
        value={formik.values.email}
        type="email"
        label="Email"
        name="email"
        placeholder="Enter your email..."
      />
      {formik.errors.email && formik.touched.email && (
        <p className="text-red-500 text-sm italic">{formik.errors.email}</p>
      )}

      <Input
        onChange={formik.handleChange}
        type="password"
        label="Password"
        name="password"
        placeholder="Enter your password..."
        value={formik.values.password}
      />
      {formik.errors.password && formik.touched.password && (
        <p className="text-red-500 text-sm italic">{formik.errors.password}</p>
      )}

      <Button type="submit" className="bg-orange-400 text-white my-5 w-full">
        Signup
      </Button>
    </form>
  );
};

export default SignUpForm;
