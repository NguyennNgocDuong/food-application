import React from "react";
import { Link } from "react-router-dom";

import SignUpForm from "./SignUpForm";

const SignUpPage = () => {
  return (
    <div className="h-screen w-full flex items-center justify-center bg-orange-400">
      <div className="w-[30%] bg-white rounded-lg shadow-lg flex flex-col p-10">
        <h1 className="text-2xl font-semibold text-center">Signup</h1>
        <SignUpForm />
        <p className="font-semibold">
          Already have an account?{" "}
          <Link className="text-orange-400" to="/login">
            Login
          </Link>
        </p>
      </div>
    </div>
  );
};

export default SignUpPage;
