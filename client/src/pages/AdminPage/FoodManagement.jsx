import React, { useEffect, useRef, useState } from "react";
import Button from "../../Components/Button/Button";
import SidebarAdmin from "../../Components/SidebarAdmin/SidebarAdmin";
import { Input, Modal, Space } from "antd";
import FormAddFood from "../../Components/FormAddFood/FormAddFood";
import FormSelect from "../../Components/FormSelect/FormSelect";

import TableComponent from "../../Components/TableComponent/TableComponent";
import { productService } from "../../services/productService";
import { FiEdit, FiTrash2, FiPlus } from "react-icons/fi";
import { toast } from "react-toastify";
import HeaderAdmin from "../../Components/HeaderAdmin/HeaderAdmin";
import { categoryService } from "../../services/categoryService";

const AdminPage = () => {
  const [valueSearch, setValueSearch] = useState("");
  const [valueFilterCategory, setValueFilterCategory] = useState({
    id: "",
    name: "",
  });

  const [open, setOpen] = useState(false);
  const [listFoods, setListFood] = useState([]);
  console.log("listFoods: ", listFoods);
  const [currentFoodSelected, setCurrentFoodSelected] = useState(null);
  const [listCategory, setListcategory] = useState([]);

  const [type, setType] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const fetchAllFood = async () => {
    const res = await productService.handleGetAllProductApi();
    setListFood(res.data.response);
  };

  const fetchAllCategory = async () => {
    const res = await categoryService.handleGetAllCategoryApi();
    setListcategory(res.data.response);
  };

  const handleDeleteFood = async (currentFoodSelected) => {
    setIsModalOpen(false);
    const res = await productService.handleDeleteProductApi(
      currentFoodSelected.id
    );
    if (res.data.err === 0) {
      toast.success(res.data.msg, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      setCurrentFoodSelected(null);
      await fetchAllFood();
    }
  };

  const renderAction = (record) => {
    const handleEditFood = (record) => {
      setType("edit");
      setOpen(true);
      setCurrentFoodSelected(record);
    };

    return (
      <div className="flex">
        <FiEdit
          onClick={() => handleEditFood(record)}
          style={{ color: "orange", fontSize: "25px", cursor: "pointer" }}
        />
        <FiTrash2
          onClick={() => {
            setIsModalOpen(true);
            setCurrentFoodSelected(record);
          }}
          style={{ color: "red", fontSize: "25px", cursor: "pointer" }}
        />
      </div>
    );
  };
  const columns = [
    {
      title: "Name",
      dataIndex: "product_name",
      filteredValue: [valueSearch],

      onFilter: (value, record) => {
        return record.product_name.toLowerCase().includes(value.toLowerCase());
      },
    },
    {
      title: "Image",
      dataIndex: "product_img",
      render: (text) => (
        <img width={70} src={`http://localhost:8080/image/${text}`} alt="" />
      ),
    },
    {
      title: "Calories",
      dataIndex: "product_calories",
      sorter: (a, b) => a.product_calories - b.product_calories,
    },
    {
      title: "Price",
      dataIndex: "product_price",
      sorter: (a, b) => a.product_price - b.product_price,

      render: (text) => <p>${text}</p>,
    },
    {
      title: "Category",
      dataIndex: ["category", "category_name"],
      filteredValue: [valueFilterCategory?.name],
      onFilter: (value, record) => {
        return record.category.category_name
          .toLowerCase()
          .includes(value.toLowerCase());
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (_, record) => {
        return renderAction(record);
      },
    },
  ];
  useEffect(() => {
    fetchAllFood();
  }, []);
  useEffect(() => {
    fetchAllCategory();
  }, []);
  return (
    <section className="flex">
      <SidebarAdmin />

      <div className="w-full ">
        <HeaderAdmin onSetValueSearch={setValueSearch} />
        <div className="px-8 py-5">
          <div className="w-full flex items-center justify-between">
            <Modal
              title="Delete Food"
              open={isModalOpen}
              footer={[
                <Button
                  className=" bg-red-500 text-white px-3 py-1 rounded-lg  hover:bg-red-600 transition-all duration-100 ease-in-out"
                  key="ok"
                  onClick={() => handleDeleteFood(currentFoodSelected)}
                >
                  OK
                </Button>,
              ]}
              onCancel={() => setIsModalOpen(false)}
            >
              <p>Are you sure you want to cancel this food?</p>
            </Modal>
            <Modal
              footer={null}
              title={type === "create" ? "Add food" : "Edit food"}
              centered
              open={open}
              onOk={() => setOpen(false)}
              onCancel={() => {
                setOpen(false);
                setCurrentFoodSelected(null);
              }}
              width={1200}
            >
              <FormAddFood
                type={type}
                onSetListFood={setListFood}
                data={currentFoodSelected}
                openModal={open}
                isOpenModal={setOpen}
                onSetCurrentFoodSelected={setCurrentFoodSelected}
              />
            </Modal>
            <h1 className="text-2xl font-semibold">List Food</h1>
            <div className="flex">
              <FormSelect
                type="category"
                options={listCategory}
                setValue={setValueFilterCategory}
                className="p-2 mr-5"
                isLabel={false}
                label="category"
              />
              <Button
                onClick={() => {
                  setOpen(true);
                  setType("create");
                }}
                className="flex items-center bg-orange-400 text-white px-2 hover:bg-orange-500 transition-all duration-100 ease-in-out"
              >
                <FiPlus />
              </Button>
            </div>
          </div>

          <TableComponent columns={columns} data={listFoods} />
        </div>
      </div>
    </section>
  );
};

export default AdminPage;
