import { Modal, Tag } from "antd";
import React, { useEffect, useMemo, useState } from "react";
import { AiFillEye } from "react-icons/ai";
import {
  FaUserAlt,
  FaTruckMoving,
  FaLocationArrow,
  FaRegListAlt,
  FaCoins,
} from "react-icons/fa";
import { toast } from "react-toastify";
import SidebarAdmin from "../../Components/SidebarAdmin/SidebarAdmin";
import HeaderAdmin from "../../Components/HeaderAdmin/HeaderAdmin";
import FormSelect from "../../Components/FormSelect/FormSelect";
import TableComponent from "../../Components/TableComponent/TableComponent";
import Button from "../../Components/Button/Button";
import { orderService } from "../../services/orderService";
import { userService } from "../../services/userService";
import moment from "moment";

const AdminPage = () => {
  const [valueSearch, setValueSearch] = useState("");
  const [valueFilterStatus, setValueFilterStatus] = useState({
    id: "",
    name: "",
  });
  const [open, setOpen] = useState(false);
  const [listOrder, setListOrder] = useState([]);
  console.log("listOrder: ", listOrder);

  const [listOrderLimit, setListOrderLimit] = useState([]);
  const [listUser, setListUser] = useState([]);

  const [currentOrder, setCurrentOrder] = useState({});
  const options = ["processing", "shipping", "delieverd"];
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      render: (text) => <p>DH{text}</p>,
      filteredValue: [valueSearch],
      onFilter: (value, record) => {
        return record.id.toString().includes(value);
      },
    },
    {
      title: "Total",
      dataIndex: "total",
      render: (text) => <p>${text}</p>,
    },
    {
      title: "Status",
      dataIndex: "status",
      filteredValue: [valueFilterStatus?.name],
      onFilter: (value, record) => {
        return record.status.toLowerCase().includes(value.toLowerCase());
      },
      render: (_, { status }) => {
        let color;
        if (status === "processing") {
          color = "red";
        }
        if (status === "shipping") {
          color = "orange";
        }
        if (status === "delieverd") {
          color = "green";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (_, record) => {
        return renderAction(record);
      },
    },
  ];
  const columnsDetail = [
    {
      title: "Image",
      dataIndex: ["Products", "product_img"],
      render: (text) => (
        <img width={50} src={`http://localhost:8080/image/${text}`} alt="" />
      ),
    },
    {
      title: "Name",
      dataIndex: ["Products", "product_name"],
    },
    {
      title: "Calories",
      dataIndex: ["Products", "product_calories"],
    },

    {
      title: "Price",
      dataIndex: ["Products", "product_price"],
      render: (text) => <p>${text}</p>,
    },
    {
      title: "Quantity",
      dataIndex: ["Products", "OrderItem", "quantity"],
    },
    {
      title: "Total",
      dataIndex: "total",
      render: (text, { Products }) => {
        return <p>${Products.product_price * Products.OrderItem.quantity}</p>;
      },
    },
  ];
  const handleAcceptOrder = async (data) => {
    setOpen(false);
    const res = await orderService.handleEditStatusOrderApi(data);
    if (res.data.err === 0) {
      toast.success(res.data.msg, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      setCurrentOrder({});
      await fetchOrderLimit();
      await fetchAllOrder();
    }
  };
  const renderAction = (record) => {
    const handleViewDetailOrder = async (record) => {
      setOpen(true);
      const res = await orderService.handleGetDetailsOrderApi(record?.id);
      setCurrentOrder({ data: res.data.response, record });
    };

    return (
      <div className="flex">
        <AiFillEye
          onClick={() => handleViewDetailOrder(record)}
          style={{ color: "black", fontSize: "25px", cursor: "pointer" }}
        />
      </div>
    );
  };

  const fetchOrderLimit = async (limit) => {
    const res = await orderService.handleGetOrderLimitApi(limit);
    setListOrderLimit(res.data.response);
  };
  const fetchAllOrder = async () => {
    const res = await orderService.handleGetAllOrderApi();
    setListOrder(res.data.response);
  };
  const fetchAllCustomer = async () => {
    const res = await userService.handleGetAllUserApi();
    setListUser(res.data.respone);
  };
  useEffect(() => {
    fetchOrderLimit(5);
  }, []);
  useEffect(() => {
    fetchAllOrder();
  }, []);

  useEffect(() => {
    fetchAllCustomer();
  }, []);

  const getTotal = useMemo(() => {
    const listOrderFilter = listOrder.filter((order) => {
      return order.payment_method === "paypal" || order.status === "delieverd";
    });
    return listOrderFilter.reduce((preVal, currentVal) => {
      return preVal + currentVal.total;
    }, 0);
  }, [listOrder]);
  console.log("getTotal: ", getTotal);

  return (
    <section className="flex">
      <Modal
        footer={[]}
        title={
          <p className="text-2xl font-semibold">DH{currentOrder.record?.id}</p>
        }
        centered
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => {
          setOpen(false);
          setCurrentOrder({});
        }}
        width={1200}
      >
        <div className="bg-orange-500 flex items-center justify-between p-3">
          <h1 className="text-white text-xl">
            {moment(currentOrder.record?.createdAt).format(
              "D MM YYYY HH:mm:ss"
            )}
          </h1>
          {currentOrder.record?.status === "processing" ? (
            <Button
              onClick={() =>
                handleAcceptOrder({
                  id: currentOrder.record?.id,
                  status: "shipping",
                })
              }
              className="bg-black px-3 font-semibold text-white hover:bg-white hover:text-black"
            >
              ACCEPT ORDER
            </Button>
          ) : (
            <Button className="bg-green-700 px-3 font-semibold text-white ">
              ORDER IS ACCEPTED
            </Button>
          )}
        </div>
        <div className="grid grid-cols-3  gap-2 mt-2">
          <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
            <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
              <FaUserAlt />
            </div>
            <div>
              <p className="font-semibold">Customer</p>
              <p>
                Name: {currentOrder.record?.users.firstName}
                {currentOrder.record?.users.lastName}
              </p>
              <p>Email: {currentOrder.record?.users.email}</p>
              <p>Phone: {currentOrder.record?.users.phoneNumber}</p>
            </div>
          </div>
          <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
            <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
              <FaTruckMoving />
            </div>
            <div>
              <p className="font-semibold">Order Info</p>
              <p>Status: {currentOrder.record?.status}</p>
              <p>Payment method: {currentOrder.record?.payment_method}</p>
            </div>
          </div>
          <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
            <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
              <FaLocationArrow />
            </div>
            <div>
              <p className="font-semibold">Deliver to</p>
              {/* <p>Address: </p> */}
              <p>{currentOrder.record?.address}</p>
            </div>
          </div>
          <div className="col-span-3">
            <TableComponent
              footer={() => (
                <p className="font-semibold text-xl text-end">
                  Total: ${currentOrder?.record?.total}
                </p>
              )}
              columns={columnsDetail}
              data={currentOrder.data}
            />
          </div>
        </div>
      </Modal>
      <SidebarAdmin />

      <div className="w-full ">
        <HeaderAdmin onSetValueSearch={setValueSearch} />
        <div className="px-8 py-5">
          <div className="flex items-center justify-between">
            <h1 className="text-2xl font-semibold">Dashboard</h1>
            <FormSelect
              type="status"
              options={options}
              setValue={setValueFilterStatus}
              className="p-2 mr-5"
              isLabel={false}
              label="status"
            />
          </div>
          <div className="grid grid-cols-3 gap-3 mt-5">
            <div className="flex items-center shadow-lg rounded-md p-5 bg-white">
              <div className=" bg-orange-300 p-5 rounded-lg">
                <FaRegListAlt className="text-orange-800 text-2xl " />
              </div>
              <div className="ml-5">
                <p className="font-semibold">{listOrder.length}</p>
                <p>Orders</p>
              </div>
            </div>
            <div className="flex items-center shadow-lg rounded-md p-5 bg-white">
              <div className=" bg-orange-300 p-5 rounded-lg">
                <FaUserAlt className="text-orange-800 text-2xl " />
              </div>
              <div className="ml-5">
                <p className="font-semibold">{listUser.length}</p>
                <p>Customers</p>
              </div>
            </div>
            <div className="flex items-center shadow-lg rounded-md p-5 bg-white">
              <div className=" bg-orange-300 p-5 rounded-lg">
                <FaCoins className="text-orange-800 text-2xl " />
              </div>
              <div className="ml-5">
                <p className="font-semibold">${getTotal}</p>
                <p>Total sales</p>
              </div>
            </div>
          </div>
          <TableComponent columns={columns} data={listOrderLimit} />
        </div>
      </div>
    </section>
  );
};

export default AdminPage;
