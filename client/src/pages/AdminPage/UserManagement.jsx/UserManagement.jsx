import { Tag } from "antd";
import React, { useEffect, useState } from "react";
import HeaderAdmin from "../../../Components/HeaderAdmin/HeaderAdmin";
import SidebarAdmin from "../../../Components/SidebarAdmin/SidebarAdmin";
import TableComponent from "../../../Components/TableComponent/TableComponent";
import { userService } from "../../../services/userService";

const UserManagement = () => {
  const [valueSearch, setValueSearch] = useState("");
  console.log("valueSearch: ", valueSearch);
  const [listUser, setListUser] = useState([]);

  const columns = [
    {
      title: "First Name",
      dataIndex: "firstName",
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
    },
    {
      title: "Email",
      dataIndex: "email",
      filteredValue: [valueSearch],
      onFilter: (value, record) => {
        return record.email.toLowerCase().includes(value.toLowerCase());
      },
    },
    {
      title: "Address",
      dataIndex: "address",
      render: (_, { address }) => {
        return address ? (
          address
        ) : (
          <Tag color="red" key={address}>
            NOT UPDATED
          </Tag>
        );
      },
    },
    {
      title: "Phone Number",
      dataIndex: "phoneNumber",
      render: (_, { phoneNumber }) => {
        return phoneNumber ? (
          phoneNumber
        ) : (
          <Tag color="red" key={phoneNumber}>
            NOT UPDATED
          </Tag>
        );
      },
    },
  ];

  const fetchAllUser = async () => {
    const res = await userService.handleGetAllUserApi();
    setListUser(res.data.respone);
  };

  useEffect(() => {
    fetchAllUser();
  }, []);

  return (
    <section className="flex">
      <SidebarAdmin />

      <div className="w-full ">
        <HeaderAdmin onSetValueSearch={setValueSearch} />
        <div className="px-8 py-5">
          <h1 className="text-2xl font-semibold">List User</h1>

          <TableComponent columns={columns} data={listUser} />
        </div>
      </div>
    </section>
  );
};

export default UserManagement;
