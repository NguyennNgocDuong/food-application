import React, { useEffect, useState } from "react";
import Input from "../../Components/Input/Input";
import * as Yup from "yup";
import { useFormik } from "formik";
import SidebarAdmin from "../../Components/SidebarAdmin/SidebarAdmin";
import Button from "../../Components/Button/Button";
import { toast } from "react-toastify";
import { categoryService } from "../../services/categoryService";
import HeaderAdmin from "../../Components/HeaderAdmin/HeaderAdmin";
import { FiEdit, FiTrash2, FiPlus, FiSave } from "react-icons/fi";
import TableComponent from "../../Components/TableComponent/TableComponent";
import { Modal } from "antd";

const CategoryManagement = () => {
  const [listCategories, setListCategories] = useState([]);
  const [valueSearch, setValueSearch] = useState("");
  const [currentCategorySelected, setCurrentCategorySelected] = useState(null);
  const [isUpdate, setIsUpdate] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const renderAction = (record) => {
    const handleEditCategory = (record) => {
      setIsUpdate(true);
      setCurrentCategorySelected(record);
    };

    return (
      <div className="flex">
        <FiEdit
          onClick={() => handleEditCategory(record)}
          style={{ color: "orange", fontSize: "25px", cursor: "pointer" }}
        />
        <FiTrash2
          onClick={() => {
            setCurrentCategorySelected(record);
            setIsModalOpen(true);
          }}
          style={{ color: "red", fontSize: "25px", cursor: "pointer" }}
        />
      </div>
    );
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Category",
      dataIndex: "category_name",
      filteredValue: [valueSearch],
      onFilter: (value, record) => {
        return record.category_name.toLowerCase().includes(value.toLowerCase());
      },
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (_, record) => {
        return renderAction(record);
      },
    },
  ];
  const formik = useFormik({
    initialValues: {
      category_name: isUpdate ? currentCategorySelected.category_name : "",
    },
    enableReinitialize: true,
    validationSchema: Yup.object({
      category_name: Yup.string().required("You must fill in this section!"),
    }),
    onSubmit: async (values) => {
      try {
        let res;

        isUpdate
          ? (res = await categoryService.handleEditCategoryApi({
              id: currentCategorySelected.id,
              category_name: values.category_name,
            }))
          : (res = await categoryService.handleAddCategoryApi({
              category_name: values.category_name,
            }));
        setIsUpdate(false);
        if (res.data.err === 0) {
          fetchCategoryApi();
          formik.values.category_name = "";
          toast.success(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        } else {
          toast.error(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      } catch (error) {
        console.log("error: ", error);
      }
    },
  });
  const handleDeleteCategory = async (id) => {
    setIsModalOpen(false);
    const res = await categoryService.handleDeleteCategoryApi(id);
    if (res.data.err === 0) {
      fetchCategoryApi();
      formik.values.category_name = "";
      toast.success(res.data.msg, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setIsUpdate(false);
  };
  const fetchCategoryApi = async () => {
    try {
      let res = await categoryService.handleGetAllCategoryApi();
      setListCategories(res.data.response);
    } catch (error) {
      console.log("error: ", error);
    }
  };

  useEffect(() => {
    fetchCategoryApi();
  }, []);

  return (
    <section className="flex">
      <SidebarAdmin />
      <Modal
        title="Delete Category"
        open={isModalOpen}
        footer={[
          <Button
            className=" bg-red-500 text-white px-3 py-1 rounded-lg  hover:bg-red-600 transition-all duration-100 ease-in-out"
            key="ok"
            onClick={() => handleDeleteCategory(currentCategorySelected.id)}
          >
            OK
          </Button>,
        ]}
        onCancel={() => setIsModalOpen(false)}
      >
        <p>Are you sure you want to delete this category?</p>
      </Modal>
      <div className="w-full">
        <HeaderAdmin onSetValueSearch={setValueSearch} />
        <div className="px-8 py-5">
          <div className="w-full flex items-center justify-between">
            <h1 className="text-2xl font-semibold">List Category</h1>

            <form className="flex items-center" onSubmit={formik.handleSubmit}>
              <input
                name="category_name"
                onChange={formik.handleChange}
                value={formik.values.category_name}
                type="text"
                placeholder="Enter category name..."
                className="px-3 py-1 border-2 rounded-md mr-3 focus:outline-orange-400 relative"
              />
              {formik.errors.category_name && formik.touched.category_name && (
                <p className="text-red-500 text-sm italic absolute top-[118px]">
                  {formik.errors.category_name}
                </p>
              )}

              {isUpdate ? (
                <Button
                  type="submit"
                  className="flex items-center bg-orange-400 text-white px-2 hover:bg-orange-500 transition-all duration-100 ease-in-out"
                >
                  <FiSave />
                </Button>
              ) : (
                <Button
                  type="submit"
                  className="flex items-center bg-orange-400 text-white px-2 hover:bg-orange-500 transition-all duration-100 ease-in-out"
                >
                  <FiPlus />
                </Button>
              )}
            </form>
          </div>

          <TableComponent columns={columns} data={listCategories} />
        </div>
      </div>
    </section>
  );
};

export default CategoryManagement;
