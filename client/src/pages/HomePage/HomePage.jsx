import { getAuth } from "firebase/auth";
import { doc, getDoc } from "firebase/firestore";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Header from "../../Components/Header/Header";
import MainContainer from "../../Components/MainContainer/MainContainer";
import { firestore } from "../../firebase/firebaseConfig";
import { set_user } from "../../redux/slice/authSlice";
import AdminPage from "../AdminPage/AdminPage";
import MenuPage from "../MenuPage/MenuPage";

import Introduce from "./Introduce";
import SlideFood from "./SlideFood";

const HomePage = () => {
  const navigate = useNavigate();
  const user = useSelector((state) => state.authSlice.currentUser);

  return user?.role === "admin" ? (
    <AdminPage />
  ) : (
    <>
      <Header />
      <MainContainer>
        <div className="w-full h-auto flex flex-col items-center justify-center">
          <Introduce />
          {/* <SlideFood /> */}
          <MenuPage />
        </div>
      </MainContainer>
    </>
  );
};

export default HomePage;
