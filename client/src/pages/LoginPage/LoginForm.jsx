import { useFormik } from "formik";
import * as Yup from "yup";
import React from "react";
import Button from "../../Components/Button/Button";
import Input from "../../Components/Input/Input";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";
import { set_user } from "../../redux/slice/authSlice";

const LoginForm = () => {
  const dispath = useDispatch();
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid Email")
        .required("You must fill in this section!"),
      password: Yup.string()
        .min(8, "Your password must be at least 8 characters!")
        .required("You must fill in this section!"),
    }),
    onSubmit: async (values) => {
      try {
        let res = await userService.handleLoginApi({
          email: values.email,
          password: values.password,
        });

        // console.log("res: ", res);
        dispath(set_user(res.data.user));
        if (res.data.err === 0) {
          toast.success(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          navigate("/");
        } else {
          toast.error(res.data.msg, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      } catch (error) {
        console.log("error: ", error);
      }
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Input
        name="email"
        onChange={formik.handleChange}
        value={formik.values.email}
        type="email"
        label="Email"
        placeholder="Enter your email..."
      />
      {formik.errors.email && formik.touched.email && (
        <p className="text-red-500 text-sm italic">{formik.errors.email}</p>
      )}
      <Input
        name="password"
        onChange={formik.handleChange}
        value={formik.values.password}
        type="password"
        label="Password"
        placeholder="Enter your password..."
      />
      {formik.errors.password && formik.touched.password && (
        <p className="text-red-500 text-sm italic">{formik.errors.password}</p>
      )}
      <Button
        type="submit"
        className="w-full bg-orange-400 text-white my-5 hover:bg-orange-500 transition-all duration-100 ease-in-out"
      >
        Login
      </Button>
    </form>
  );
};

export default LoginForm;
