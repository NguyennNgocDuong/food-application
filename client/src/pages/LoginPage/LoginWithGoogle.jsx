import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Button from "../../Components/Button/Button";
import { app } from "../../firebase/firebaseConfig";
import { set_user } from "../../redux/slice/authSlice";
import { userLocal } from "../../services/localServices";
import { FaGoogle } from "react-icons/fa";

const LoginWithGoogle = () => {
  const user = useSelector((state) => state.authSlice.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // firebase
  const firebaseAuth = getAuth(app);
  const provider = new GoogleAuthProvider();
  // sign in with google firebase
  const login = async () => {
    if (!user) {
      const {
        user: { providerData },
      } = await signInWithPopup(firebaseAuth, provider);
      console.log("providerData: ", providerData[0]);
      const action = set_user(providerData[0]);
      dispatch(action);
      userLocal.set(providerData[0]);
      navigate("/");
    }
  };
  return (
    <Button
      onClick={login}
      className="flex items-center justify-center bg-gray-500 text-white hover:bg-gray-600 transition-all duration-100 ease-in-out"
    >
      <FaGoogle className="mr-2" />
      Sign in with Google
    </Button>
  );
};

export default LoginWithGoogle;
