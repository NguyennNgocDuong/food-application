import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { userLocal } from "../../services/localServices";
import { useNavigate } from "react-router-dom";

import LoginForm from "./LoginForm";
import LoginWithGoogle from "./LoginWithGoogle";

const LoginPage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    let userInfo = userLocal.get();
    userInfo && navigate("/");
  }, []);

  return (
    <div className="h-screen w-full flex items-center justify-center bg-orange-400">
      <div className="w-[30%] bg-white rounded-lg shadow-lg flex flex-col p-10">
        <h1 className="text-2xl font-semibold text-center">Login</h1>
        <LoginForm />
        <p className="font-semibold">
          Already have an account?{" "}
          <Link className="text-orange-400" to="/sign-up">
            Sign Up
          </Link>
        </p>
        {/* <p className="flex items-center justify-center my-5 text-slate-400">
          or
        </p>
        <LoginWithGoogle /> */}
      </div>
    </div>
  );
};

export default LoginPage;
