import { motion } from "framer-motion";
import { IoFastFood } from "react-icons/io5";
import React, { useEffect, useState } from "react";
import { categories } from "../../utils/data";
import ListFood from "../../Components/ListFood/ListFood";
import { useDispatch, useSelector } from "react-redux";
import { getAllFoodItems } from "../../firebase/firebaseFunction";
import { set_foodItem } from "../../redux/slice/foodItemSlice";
import Header from "../../Components/Header/Header";
import MainContainer from "../../Components/MainContainer/MainContainer";
import { categoryService } from "../../services/categoryService";
import { productService } from "../../services/productService";

const MenuPage = () => {
  const [filter, setFilter] = useState(0);
  const [listCategories, setListCategories] = useState([]);

  const [listFoodCategory, setListFoodCategory] = useState([]);

  const fetchCategoryApi = async () => {
    try {
      let res = await categoryService.handleGetAllCategoryApi();
      setListCategories(res.data.response);
    } catch (error) {
      console.log("error: ", error);
    }
  };

  const fetchProductCategoryApi = async () => {
    const params = {
      category: filter,
    };

    try {
      let res;
      if (filter === 0) {
        res = await productService.handleGetAllProductApi();
      } else {
        res = await productService.handleGetProductCategoryApi(params);
      }
      setListFoodCategory(res.data.response);
    } catch (error) {
      console.log("error: ", error);
    }
  };

  useEffect(() => {
    fetchProductCategoryApi();
  }, [filter]);

  useEffect(() => {
    fetchCategoryApi();
  }, []);

  return (
    <>
      {/* <Header /> */}
      <section className="w-full " id="menu">
        <div className="w-full flex flex-col items-center justify-center">
          <p className="text-2xl font-semibold capitalize text-headingColor relative before:absolute before:rounded-lg before:content before:w-16 before:h-1 before:-bottom-2 before:left-0 before:bg-gradient-to-tr from-orange-400 to-orange-600 transition-all ease-in-out duration-100 mr-auto">
            Our Hot Dishes
          </p>

          <div className="w-full flex items-center justify-start lg:justify-center gap-8 py-6 overflow-x-scroll scrollbar-none">
            <motion.div
              whileTap={{ scale: 0.75 }}
              className={`group ${
                filter === 0 ? "bg-cartNumBg" : "bg-card"
              } w-24 min-w-[94px] h-28 cursor-pointer rounded-lg drop-shadow-xl flex flex-col gap-3 items-center justify-center hover:bg-cartNumBg `}
              onClick={() => setFilter(0)}
            >
              <div
                className={`w-10 h-10 rounded-full shadow-lg ${
                  filter === 0 ? "bg-white" : "bg-cartNumBg"
                } group-hover:bg-white flex items-center justify-center`}
              >
                <IoFastFood
                  className={`${
                    filter === 0 ? "text-textColor" : "text-white"
                  } group-hover:text-textColor text-lg`}
                />
              </div>
              <p
                className={`text-sm ${
                  filter === 0 ? "text-white" : "text-textColor"
                } group-hover:text-white`}
              >
                All
              </p>
            </motion.div>
            {listCategories?.map((category) => (
              <motion.div
                whileTap={{ scale: 0.75 }}
                key={category.id}
                className={`group ${
                  filter === category.id ? "bg-cartNumBg" : "bg-card"
                } w-24 min-w-[94px] h-28 cursor-pointer rounded-lg drop-shadow-xl flex flex-col gap-3 items-center justify-center hover:bg-cartNumBg `}
                onClick={() => setFilter(category.id)}
              >
                <div
                  className={`w-10 h-10 rounded-full shadow-lg ${
                    filter === category.id ? "bg-white" : "bg-cartNumBg"
                  } group-hover:bg-white flex items-center justify-center`}
                >
                  <IoFastFood
                    className={`${
                      filter === category.id ? "text-textColor" : "text-white"
                    } group-hover:text-textColor text-lg`}
                  />
                </div>
                <p
                  className={`text-sm ${
                    filter === category.id ? "text-white" : "text-textColor"
                  } group-hover:text-white`}
                >
                  {category.category_name}
                </p>
              </motion.div>
            ))}
          </div>

          <ListFood flag={false} data={listFoodCategory} />
        </div>
      </section>
    </>
  );
};

export default MenuPage;
