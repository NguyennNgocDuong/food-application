import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Header from "../../Components/Header/Header";
import MainContainer from "../../Components/MainContainer/MainContainer";
import { orderService } from "../../services/orderService";
import { Modal, Space, Table, Tag } from "antd";
import { toast } from "react-toastify";
import { FaUserAlt, FaTruckMoving, FaLocationArrow } from "react-icons/fa";
import TableComponent from "../../Components/TableComponent/TableComponent";
// import Button from "../../../Components/Button/Button";
import Button from "../../Components/Button/Button";
import { AiFillEye } from "react-icons/ai";
import { FiTrash2 } from "react-icons/fi";
import moment from "moment";

const OrderPage = () => {
  const user = useSelector((state) => state.authSlice.currentUser);
  const [listOrder, setListOrder] = useState([]);

  const [open, setOpen] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentOrder, setCurrentOrder] = useState({});

  const columnsDetail = [
    {
      title: "Image",
      dataIndex: ["Products", "product_img"],
      render: (text) => (
        <img width={50} src={`http://localhost:8080/image/${text}`} alt="" />
      ),
    },
    {
      title: "Name",
      dataIndex: ["Products", "product_name"],
    },
    {
      title: "Calories",
      dataIndex: ["Products", "product_calories"],
    },

    {
      title: "Price",
      dataIndex: ["Products", "product_price"],
      render: (text) => <p>${text}</p>,
    },
    {
      title: "Quantity",
      dataIndex: ["Products", "OrderItem", "quantity"],
    },
    {
      title: "Total",
      dataIndex: "total",
      render: (text, { Products }) => {
        return <p>${Products.product_price * Products.OrderItem.quantity}</p>;
      },
    },
  ];

  const fetchOrderApi = async () => {
    try {
      const res = await orderService.handleGetOrderApi(user?.id);
      setListOrder(res.data.response);
    } catch (error) {
      console.log("error: ", error);
    }
  };

  const handleViewDetails = async (record) => {
    const { id } = record;
    const res = await orderService.handleGetDetailsOrderApi(id);

    setCurrentOrder({ data: res.data.response, record });
    setOpen(true);
  };

  const handleCancelOrder = async (record) => {
    setIsModalOpen(false);
    const { id, status } = record;
    if (status === "processing") {
      const res = await orderService.handleDeleteOrderApi(id);
      if (res.data.err === 0) {
        toast.success(res.data.msg, {
          position: "top-right",
          autoClose: 1000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        await fetchOrderApi();
      }
    } else {
      toast.error(`Order is ${status}! Can't delete order`, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <p>DH{text}</p>,
    },

    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (_, { status }) => {
        // let color = tag.length > 5 ? "geekblue" : "green";
        let color;
        if (status === "processing") {
          color = "red";
        }
        if (status === "shipping") {
          color = "orange";
        }
        if (status === "delieverd") {
          color = "green";
        }
        return (
          <>
            <Tag color={color} key={status}>
              {status.toUpperCase()}
            </Tag>
          </>
        );
      },
    },
    {
      title: "Total",
      dataIndex: "total",
      key: "total",
      render: (text) => <p>${text}</p>,
    },
    {
      title: "Payment method",
      dataIndex: "payment_method",
      key: "payment_method",
    },
    {
      title: "Action",
      key: "action",
      render: (record) => (
        <Space size="middle">
          <FiTrash2
            style={{ color: "black", fontSize: "25px", cursor: "pointer" }}
            onClick={() => {
              setIsModalOpen(true);
              setCurrentOrder(record);
            }}
          />

          <>
            <AiFillEye
              onClick={() => handleViewDetails(record)}
              style={{ color: "black", fontSize: "25px", cursor: "pointer" }}
            />
          </>
        </Space>
      ),
    },
  ];
  const handleAcceptOrder = async (data) => {
    setOpen(false);
    const res = await orderService.handleEditStatusOrderApi(data);
    if (res.data.err === 0) {
      toast.success(res.data.msg, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      setCurrentOrder({});
      await fetchOrderApi();
    }
  };
  useEffect(() => {
    fetchOrderApi();
  }, []);

  return (
    <>
      <Header />
      <MainContainer>
        <Modal
          title="Delete Order"
          open={isModalOpen}
          footer={[
            <Button
              className=" bg-red-500 text-white px-3 py-1 rounded-lg  hover:bg-red-600 transition-all duration-100 ease-in-out"
              key="ok"
              onClick={() => handleCancelOrder(currentOrder)}
            >
              OK
            </Button>,
          ]}
          onCancel={() => setIsModalOpen(false)}
        >
          <p>Are you sure you want to cancel the order?</p>
        </Modal>
        <Modal
          footer={[]}
          title={
            <p className="text-2xl font-semibold">
              DH{currentOrder.record?.id}
            </p>
          }
          centered
          open={open}
          onOk={() => setOpen(false)}
          onCancel={() => {
            setOpen(false);
            setCurrentOrder({});
          }}
          width={1200}
        >
          <div className="bg-orange-500 flex items-center justify-between p-3">
            <h1 className="text-white text-xl">
              {moment(currentOrder.record?.createdAt).format(
                "D MM YYYY HH:mm:ss"
              )}
            </h1>
            {currentOrder.record?.status === "shipping" ? (
              <Button
                onClick={() =>
                  handleAcceptOrder({
                    id: currentOrder.record?.id,
                    status: "delieverd",
                  })
                }
                className="bg-black px-3 font-semibold text-white hover:bg-white hover:text-black"
              >
                COMPLETE ORDER
              </Button>
            ) : currentOrder.record?.status === "processing" ? (
              <Button className="bg-green-700  px-3 font-semibold text-white ">
                ORDER IS PROCESSING
              </Button>
            ) : (
              <Button className="bg-green-700 px-3 font-semibold text-white ">
                ORDER IS COMPLETED
              </Button>
            )}
          </div>
          <div className="grid grid-cols-3  gap-2 mt-2">
            <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
              <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
                <FaUserAlt />
              </div>
              <div>
                <p className="font-semibold">Customer</p>
                <p>
                  Name: {user.firstName}
                  {user.lastName}
                </p>
                <p>Email: {user.email}</p>
                <p>Phone: {user.phoneNumber}</p>
              </div>
            </div>
            <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
              <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
                <FaTruckMoving />
              </div>
              <div>
                <p className="font-semibold">Order Info</p>
                <p>Status: {currentOrder.record?.status}</p>
                <p>Payment method: {currentOrder.record?.payment_method}</p>
              </div>
            </div>
            <div className="flex items-center shadow-lg rounded-md p-3 bg-gray-200">
              <div className="w-10 h-10 flex items-center justify-center bg-orange-200 text-orange-500 rounded-full mr-2">
                <FaLocationArrow />
              </div>
              <div>
                <p className="font-semibold">Deliver to</p>
                {/* <p>Address: </p> */}
                <p>{currentOrder.record?.address}</p>
              </div>
            </div>
            <div className="col-span-3">
              <TableComponent
                footer={() => (
                  <p className="font-semibold text-xl text-end">
                    Total: ${currentOrder?.record?.total}
                  </p>
                )}
                columns={columnsDetail}
                data={currentOrder.data}
              />
            </div>
          </div>
        </Modal>
        <section className="w-full my-6">
          <p className="text-2xl font-semibold relative after:absolute after:rounded-lg after:content after:w-full after:h-0.5 after:-bottom-2 after:left-0 after:bg-gradient-to-tr from-orange-400 to-orange-600 transition-all ease-in-out duration-100 mb-5">
            My Order
          </p>

          <Table
            pagination={{
              position: [],
            }}
            rowKey={(record) => record.id}
            columns={columns}
            dataSource={listOrder}
          />
        </section>
      </MainContainer>
    </>
  );
};

export default OrderPage;
