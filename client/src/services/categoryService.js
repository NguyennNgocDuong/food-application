import { https } from './axios'
export const categoryService = {
    handleAddCategoryApi: (data) => {
        let url = "/api/v1/add-category"
        return https.post(url, data)
    },
    handleGetAllCategoryApi: () => {
        let url = "/api/v1/get-category"
        return https.get(url)
    },
    handleEditCategoryApi: (data) => {
        let url = "/api/v1/edit-category"
        return https.put(url, data)
    },
    handleDeleteCategoryApi: (params) => {
        let url = `/api/v1/delete-category/${params}`
        return https.delete(url)
    },

}

