import { https } from './axios'

export const orderService = {
    handleOrderApi: (data) => {
        let url = "/api/v1/check-out"
        return https.post(url, data)
    },
    handleGetAllOrderApi: () => {
        let url = `/api/v1/get-all-order`
        return https.get(url)
    },
    handleGetOrderLimitApi: (params) => {
        let url = `/api/v1/get-order-limit/${params}`
        return https.get(url)
    },
    handleGetOrderApi: (params) => {
        let url = `/api/v1/get-order-user/${params}`
        return https.get(url)
    },
    handleGetDetailsOrderApi: (params) => {
        let url = `/api/v1/get-order-detail/${params}`
        return https.get(url)
    },
    handleDeleteOrderApi: (params) => {
        let url = `/api/v1/delete-order/${params}`
        return https.delete(url)
    },
    handleEditStatusOrderApi: (data) => {
        let url = `/api/v1/edit-status-order`
        return https.put(url, data)
    },



}

