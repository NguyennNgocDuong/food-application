import axios from "axios"

export const getApiProvincePublic = async () => {
    return await axios({
        method: "GET",
        url: "https://vapi.vnappmob.com/api/province/"
    })
}

export const getApiDistrictPublic = async (provinceId) => {
    return await axios({
        method: "GET",
        url: `https://vapi.vnappmob.com/api/province/district/${provinceId}`
    })
}
export const getApiWardPublic = async (districtId) => {
    return await axios({
        method: "GET",
        url: `https://vapi.vnappmob.com/api/province/ward/${districtId}`
    })
}