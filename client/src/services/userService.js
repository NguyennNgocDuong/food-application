import { https } from './axios'
export const userService = {

    handleRefreshTokenApi: (id) => {
        let url = "/api/v1/refresh-token"
        return https.post(url, id)
    },
    handleLoginApi: (data) => {
        let url = "/api/v1/login"
        return https.post(url, data)
    },
    handleSignUpApi: (data) => {
        const url = "/api/v1/register"
        return https.post(url, data)
    },
    handleGetCurrentUserApi: () => {
        const url = "/api/v1/get-current-user"
        return https.get(url)
    },
    handleGetAllUserApi: () => {
        const url = "/api/v1/get-all-user"
        return https.get(url)
    },
    handleUpdateUserInfoApi: (data) => {
        const url = "/api/v1/edit-user"
        return https.put(url, data)

    }

}


