
import axios from "axios";
import { userLocal } from "./localServices";
import jwt_decode from "jwt-decode";
import { userService } from "./userService";

export const config = {
    headers: {
        "Content-Type": "multipart/form-data"
    }
}

export const https = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL,
});
// Add a request interceptor
https.interceptors.request.use(async function (config) {
    // Do something before request is sent
    // // check han su dung token
    // const date = new Date()
    // const decodedToken = jwt_decode(user.accessToken)
    // if (decodedToken < date.getTime() / 1000) {
    //     const data = await userService.handleRefreshTokenApi(user.id)
    //     const refreshUser = {
    //         ...user, accessToken: data.accessToken, refreshToken: data.refreshToken
    //     }
    // }
    const user = userLocal.get()

    // gắn token vào header
    config.headers = {
        authorization: user ? `Bearer ${user.accessToken}` : null
    }

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
https.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    // refresh token
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});


