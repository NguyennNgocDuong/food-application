import { config, https } from './axios'





export const productService = {
    handleAddProductApi: (data) => {
        let url = "/api/v1/add-product"
        return https.post(url, data, config)
    },
    handleGetProductCategoryApi: (params) => {
        let url = `/api/v1/get-product/${params.category}`
        return https.get(url)
    },
    handleGetAllProductApi: () => {
        let url = `/api/v1/get-all-product`
        return https.get(url)
    },
    handleUpdateProductApi: (data) => {
        let url = `/api/v1/update-product`
        return https.put(url, data, config)
    },
    handleDeleteProductApi: (params) => {
        let url = `/api/v1/delete-product/${params}`
        return https.delete(url)
    },

}

