const USER_INFOR = 'USER_INFOR';
const USER_CART = 'USER_CART';
export const userLocal = {
    set: (inforUser) => {
        const json = JSON.stringify(inforUser);
        localStorage.setItem(USER_INFOR, json);
    },
    get: () => {
        const json = localStorage.getItem(USER_INFOR);
        if (json) {
            return JSON.parse(json);
        } else {
            return null
        }
    },
    remove: () => {
        localStorage.removeItem(USER_INFOR);
    }
}

export const cartLocal = {
    set: (cart, uid) => {
        const json = JSON.stringify(cart);
        localStorage.setItem(`${USER_CART}${uid}`, json);
    },
    get: (uid) => {
        const json = localStorage.getItem(`${USER_CART}${uid}`);
        if (json) {
            return JSON.parse(json);
        } else {
            return null
        }
    },
    remove: (uid) => {
        localStorage.removeItem(`${USER_CART}${uid}`);
    }
}