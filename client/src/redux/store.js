import { configureStore } from '@reduxjs/toolkit'
import authSlice from './slice/authSlice'
import foodItemSlice from './slice/foodItemSlice'
import loadingSlice from './slice/loadingSlice'
import cartSlice from './slice/cartSlice'

export const store = configureStore({
    reducer: {
        authSlice,
        foodItemSlice,
        loadingSlice,
        cartSlice
    },
})