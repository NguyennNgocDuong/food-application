import { createSlice } from '@reduxjs/toolkit'
import { cartLocal, userLocal } from '../../services/localServices';

const user = userLocal.get()



const initialState = {

  listCarts: cartLocal.get(user?.id) || []
}




const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {

    add_to_cart: (state, action) => {
      const { id, uid } = action.payload

      const indexCart = state.listCarts.find((item) => item.id === id)
      if (indexCart) {
        indexCart.qty += 1

      } else {
        state.listCarts.push({ ...action.payload, qty: 1 })

      }
      cartLocal.set(state.listCarts, uid);


    },
    change_quantity: (state, action) => {
      console.log('state: ', state.listCarts);
      const { id, uid, number } = action.payload
      const indexCart = state.listCarts.find((item) => item.id === id)
      if (indexCart) {
        // if (indexCart.qty > 1) {
        indexCart.qty += number
        // }
      }
      cartLocal.set(state.listCarts, uid)


    },
    remove_cart: (state, action) => {
      const { id, uid } = action.payload
      const removeItem = state.listCarts.filter((item) => item.id !== id);
      state.listCarts = removeItem;
      cartLocal.set(state.listCarts, uid)

    },
    clear_cart: (state, action) => {
      state.listCarts = []
      cartLocal.remove(user?.id);
    }


  }
});

export const { setListCartData, add_to_cart, change_quantity, remove_cart, clear_cart } = cartSlice.actions

export default cartSlice.reducer