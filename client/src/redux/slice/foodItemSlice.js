import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    foodItem: null
}

const foodItemSlice = createSlice({
    name: "foodItem",
    initialState,
    reducers: {
        set_foodItem: (state, action) => {
            state.foodItem = action.payload

        }
    }
});

export const { set_foodItem} = foodItemSlice.actions

export default foodItemSlice.reducer