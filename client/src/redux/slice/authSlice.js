import { createSlice } from '@reduxjs/toolkit'
import { userLocal } from '../../services/localServices';

const initialState = {
    currentUser: userLocal.get(),
}

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        set_user: (state, action) => {
            state.currentUser = action.payload
            userLocal.set(action.payload)
        }
    }
});

export const { set_user } = authSlice.actions

export default authSlice.reducer